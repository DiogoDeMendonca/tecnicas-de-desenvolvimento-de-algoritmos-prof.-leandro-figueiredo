#include <stdio.h>

int main(){

/* Questão 01:
C é considerada uma linguagem de médio nível. Explique.

Resposta:
Considerando que C detém a capacidade de manipulação à memória, ao hardware e contribui para aplicações análogas ao Assembly, ou seja, baixo nível, mas que ao mesmo tempo, possibilita reaproveitamento do código e tem uma sintaxe moderna, com elevado nível de abstração, temos uma característica de alto nível.

Grande parte da literatura bipolariza os níveis das linguagens de programação, entretanto, e mesmo assim, ainda podemos dizer que C seja uma Linguagem de Médio Nível, uma vez que é uma linguagem que intermedia acesso à linguagem de máquina e também à complexidade moderna dos compiladores.
*/

    return 0;
}