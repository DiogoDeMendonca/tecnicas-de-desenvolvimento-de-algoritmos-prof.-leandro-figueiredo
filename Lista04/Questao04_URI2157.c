#include <stdio.h>
#include <locale.h>

int main() {

	setlocale(LC_ALL, "portuguese");

	int i			= 0;
	int c			= 0;
	int ini			= 0;
	int fim			= 0;
	int cont		= 0;
	int contTeste	= 0;
	int num[100] 	= { };
	int reverso[100]= { };

	printf("Quantos Testes Desejas Realizar? \n");
	scanf("%d", &c);
	system("CLS");
	printf("\a");
	
	for ( contTeste = 0 ; contTeste < c ; contTeste++ ){
	
	printf("Digite o INÍCIO da Sequência #%d:\n", contTeste+1);
	scanf("%d", &ini);
	system("CLS");
	printf("\a");
	
	printf("Digite o FIM da Sequência: #%d:\n", contTeste+1);
	scanf("%d", &fim);
	system("CLS");
	printf("\a");
	
		for ( cont = 0, i = ini ; i <= fim ; i++, cont++ ){
			num[cont] = i;
			printf("%d", num[cont]);
		}
		
		for ( cont = 0, i = ini ; i <= fim ; i++, cont++ ){
			reverso[i] = num[fim - i];
			printf("%d", reverso[i]);
		}
		
	printf("\a\n");		
	}

    return 0;
}