#include <stdio.h>
#include <string.h>
#include <locale.h>

int main() {

	setlocale(LC_ALL, "portuguese");

	int i				= 0;
	int n1				= 0;
	int n2				= 0;
	int soma			= 0;
	char strSoma[100]	= { };
	
	printf("Digite N1:\n");
	scanf("%d", &n1);
	system("CLS");
	printf("\a");
	
	printf("Digite N2:\n");
	scanf("%d", &n2);
	system("CLS");
	printf("\a");
	
	soma = n1 + n2;
	
	sprintf(strSoma, "%i", soma);

	for ( i ; i < strlen(strSoma) ; i++ ){
		
		if (strSoma[i] != '0'){
			printf("%c", strSoma[i]);
		}
	}

    return 0;
}