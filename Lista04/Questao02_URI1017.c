#include <stdio.h>
 
int main() {
 
	double tempo;
	double velocidade;
	double litros;

	scanf("%lf", &tempo);
    scanf("%lf", &velocidade);

	litros = ((tempo * velocidade) / 12);
	printf("%.3lf\n", litros);
    
    return 0;
}