int main(){

	int cont = 0;
	int elementos = 0;
	int razao = 0;
	int PG = 1;
	
	printf("..:: PG - Progressao Geometrica ::.. \n \n");
	printf("Digite a Quantidade de Elementos Para Sua PG: \n \n");
	scanf("%d" , &elementos);
	printf("\a");
	system("CLS");

	printf("..:: PG - Progressao Geometrica ::.. \n \n");
	printf("Digite a Razao de Sua PG: \n \n");
	scanf("%d" , &razao);
	printf("\a");
	system("CLS");

	printf("..:: PG - Progressao Geometrica ::.. \n \n");
	printf("RAZAO: %d \n" , razao);
	printf("ELEMENTOS: %d \n \n" , elementos);

	while (cont <= elementos){	
		printf("\t %d \n" , PG);
		PG = PG * razao;
		cont++;		
		}
	
	printf("\a \a");
	
	return 0;
}