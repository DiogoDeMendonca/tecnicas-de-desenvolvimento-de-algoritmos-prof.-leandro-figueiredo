#include <stdio.h>

int main(){

	int num = 0;
	
	printf("PAR ou IMPAR? \n");
	printf("Digite Um Numero Inteiro Para Consultar: \n \n");
	scanf("%d" , &num);
	
	system("CLS");

	printf("\a");

	printf("Voce Digitou: %d \n \n" , num);

	if ( num % 2 == 0 ) {
		printf("===============\n");
		printf("=    P A R    =\n");
		printf("===============\n");
	} else {
		printf("===============\n");
		printf("=  I M P A R  =\n");
		printf("===============\n");		
	}

	return 0;
}