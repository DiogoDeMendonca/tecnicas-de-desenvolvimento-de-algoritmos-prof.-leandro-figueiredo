#include <stdio.h>

int main(){

	int ano = 0;
	
	printf("..:: ANO BISSEXTO ::.. \n \n");
	printf("Digite o ANO a ser verificado: \n \n");
	scanf("%d", &ano);
	system("CLS");
	printf("\a");
	
	printf("================\n");
	printf("=     %d     =\n", ano);
	
	if (ano % 4 == 0){
		printf("================\n");
		printf("=   BISSEXTO   =\n");
		printf("================\n");
	} else {
		printf("================\n");
		printf("= NAO BISSEXTO =\n");
		printf("================\n");
	}
	
	printf("\a \a");
	
	return 0;
}