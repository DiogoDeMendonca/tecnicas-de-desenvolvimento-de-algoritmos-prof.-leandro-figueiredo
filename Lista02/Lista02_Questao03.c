#include <stdio.h>

int main(){

	int num = 0;
	int fat = 1;
	
	printf("= F A T O R I A L ! =\n");
	printf("Digite Um Numero Inteiro Para Calcular: \n \n");
	scanf("%d" , &num);
	
	system("CLS");

	printf("\a");

	if (num <= 0) {
		fat = 0;
		printf("O Calculo de Fatorial nao admite numeros negativos! \n \n");
	} else {
		while (num > 1){	
		fat = fat * num;
		num--;		
		}
	}
	
	printf("RESULTADO: %d" , fat);
	
	return 0;
}