#include <stdio.h>

int main(){

	int cont = 0;
	int elementos = 0;
	int razao = 0;
	int PA = 0;
	
	printf("..:: PA - Progressao Aritmetica ::.. \n \n");
	printf("Digite a Quantidade de Elementos Para Sua PA: \n \n");
	scanf("%d" , &elementos);
	printf("\a");
	system("CLS");

	printf("..:: PA - Progressao Aritmetica ::.. \n \n");
	printf("Digite a Razao de Sua PA: \n \n");
	scanf("%d" , &razao);
	printf("\a");
	system("CLS");

	printf("..:: PA - Progressao Aritmetica ::.. \n \n");
	printf("RAZAO: %d \n" , razao);
	printf("ELEMENTOS: %d \n \n" , elementos);

	while (cont <= elementos){	
		printf("\t %d \n" , PA);
		PA = PA + razao;
		cont++;		
		}
	
	printf("\a \a");
	
	return 0;
}