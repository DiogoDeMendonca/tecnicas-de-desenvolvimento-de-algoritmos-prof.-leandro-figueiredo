#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <windows.h>

	char a;
	int posX = 7;
	int posY = 21;

void GoToXY(int posX, int posY, char string[100]){
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD coordenadas = { posX, posY };
    SetConsoleCursorPosition(hStdout, coordenadas);
    printf("%s", string);    
}

void menu(){
	
	GoToXY( 9, 21, "Jogar");
	GoToXY(21, 21, "Conquistas");
	GoToXY(38, 21, "Op��es");
	GoToXY(51, 21, "Cr�ditos");
	GoToXY(66, 21, "Sair        ");
}

int main(){
	
	//system("color 0F");

	// ############## V A R I A V E I S #################
	
	int n = 0;
	int i = 0;

	// ############## C A B E C A L H O #################
	
	if(1){
		
		// Linha Superior:
		printf(" ");
		for ( n = 0 ; n < 78 ; n++ ){
			printf("%c", 220);
		}
		
		printf("\n");
	
		for ( n = 0 ; n < 1 ; n++ ){
			printf(" %c                                                                            %c\n", 186, 186);
		}
	
		printf(" %c                            ..:: INTELLECTUS ::..                           %c\n", 186, 186);
		printf(" %c                         Desafie Seus Conhecimentos                         %c\n", 186, 186);
	
		for ( n = 0 ; n < 1 ; n++ ){
			printf(" %c                                                                            %c\n", 186, 186);
		}
	
		// Linha Inferior:
		printf(" ");			
		for ( n = 0 ; n < 78 ; n++ ){
			printf("%c", 223);
		}			
		printf("\n");
	}
	
	// Linha Superior COMPLETA:			
	if(1){
		for ( n = 0 ; n < 80 ; n++ ){
			printf("%c", 205);
		}	
	}

	// ################# C O R P O #################
	
	if(1){
		
		printf("                                                                               \n"); // 01
		printf("                                                                               \n"); // 02
		
		printf("   %c%c%c%c%c %c%c  %c%c %c%c%c%c%c %c%c%c%c%c%c %c%c     %c%c     %c%c%c%c%c%c   %c%c%c%c  %c%c%c%c%c %c%c  %c%c  %c%c%c%c   \n", 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219 ); // 03 -39
		printf("     %c   %c%c%c %c%c   %c   %c%c     %c%c     %c%c     %c%c      %c%c  %c%c   %c   %c%c  %c%c %c%c  %c   \n", 219, 219, 219, 220, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219); // 04 - 26
		printf("     %c   %c%c%c%c%c%c   %c   %c%c     %c%c     %c%c     %c%c      %c%c       %c   %c%c  %c%c %c%c      \n", 219, 219, 219, 219, 220, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219); // 05 - 23
		printf("     %c   %c%c%c%c%c%c   %c   %c%c%c%c%c  %c%c     %c%c     %c%c%c%c%c   %c%c       %c   %c%c  %c%c  %c%c%c%c   \n", 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219,219); // 06 - 28
		printf("     %c   %c%c%c%c%c%c   %c   %c%c     %c%c     %c%c     %c%c      %c%c       %c   %c%c  %c%c     %c%c  \n", 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219); // 07 - 23
		printf("     %c   %c%c %c%c%c   %c   %c%c     %c%c     %c%c     %c%c      %c%c  %c%c   %c   %c%c  %c%c %c%c  %c%c  \n", 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219); // 08 - 26
		printf("   %c%c%c%c%c %c%c  %c%c   %c   %c%c%c%c%c%c %c%c%c%c%c%c %c%c%c%c%c%c %c%c%c%c%c%c   %c%c%c%c    %c    %c%c%c%c   %c%c%c%c   \n", 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219); // 09 - 41
		
		printf("                                                                               \n"); // 10
		printf("                                                                               \n"); // 11
	}
	
	// Linha Inferior COMPLETA:
	if(1){
		setlocale(LC_ALL, "C");
		//printf(" ");
		for ( n = 0 ; n < 80 ; n++ ){
			printf("%c", 205);
		}
	}

	// ################# R O D A P E #################

	// Inicio Retangulo:
		
		
			// Linha Superior:
			printf(" ");
			for ( n = 0 ; n < 78 ; n++ ){
				printf("%c", 220);
			}
			
			printf("\n");
		
			for ( n = 0 ; n < 2 ; n++ ){
				printf(" %c                                                                            %c\n", 186, 186);
			}
			
	  		//printf(" %c", 186);
	  				
	// ################# M E N U #################
			
							
			setlocale(LC_ALL, "C");
			//printf("%c\n", 186);
						
			for ( n = 0 ; n < 1 ; n++ ){
				printf(" %c                                                                            %c\n", 186, 186);
			}
			
			// Linha Inferior:
			printf(" ");			
			for ( n = 0 ; n < 78 ; n++ ){
				printf("%c", 223);
			}		
			
			printf("\n");
		
		do {	
		
			setlocale(LC_ALL, "portuguese");
			menu();

			GoToXY( posX, posY, ">");
			
			a = toupper(getch());
			
			switch(a){
			
			case 77 :
				// Jogar:
				if ( posX == 7 ){
                    GoToXY( posX, posY, " ");
                    posX = 19;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(500, 100);
                } else 
                
                // Conquistas:
                if ( posX == 19 ){
                    GoToXY( posX, posY, " ");
                    posX = 36;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(500, 100);
                } else 
                
                // Op��es:
                if ( posX == 36 ){
                    GoToXY( posX, posY, " ");
                    posX = 49;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(500, 100);
                } else
                
                // Cr�ditos:
                if ( posX == 49 ){
                    GoToXY( posX, posY, " ");
                    posX = 64;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(500, 100);
                } else 
                
                // Sair:
                if ( posX == 64 ){
                    GoToXY( posX, posY, " ");
                    posX = 7;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(500, 100);
                }
        	break;
        	
        	case 75 :
        		// Sair:
                if ( posX == 64 ){
                    GoToXY( posX, posY, " ");
                    posX = 49;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(400, 100);
                } else
                
                // Cr�ditos:
                if ( posX == 49 ){
                    GoToXY( posX, posY, " ");
                    posX = 36;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(400, 100);
                } else
                
                // Op��es:
                if ( posX == 36 ){
                    GoToXY( posX, posY, " ");
                    posX = 19;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(400, 100);
                } else
                
                // Conquistas:
                if ( posX == 19 ){
                    GoToXY( posX, posY, " ");
                    posX = 7;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(400, 100);
                } else 
                
                // Jogar:
				if ( posX == 7 ){
                    GoToXY( posX, posY, " ");
                    posX = 64;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(400, 100);
                }
			break; 
			
			case 13:
				if ( posX == 64 ){
					system("CLS");
                    exit(0);
            	}
            break;
						
			//default:
			
			
		}
			
			
		} while (1);

	return 0;
}
