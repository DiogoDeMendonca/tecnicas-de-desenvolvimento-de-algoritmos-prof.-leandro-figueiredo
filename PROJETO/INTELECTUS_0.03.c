#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>		// Configurar a linguagem
#include <windows.h>	// Personalizar as cores do CMD

int main(){
	setlocale(LC_ALL, "portuguese"); 
	
									// +++++++++++++++++++++++++++
									//  DECLARA��O DE VARI�VEIS: + 
									// +++++++++++++++++++++++++++

	char nomeJogador[100] 	= "osama";	// Receber� o nome do Jogador;
	float pontos 			= 1000.0;	// Receber� a pontua��o do Jogador ao decorrer do jogo;
	float erro 				= 999.0;  // Decremento da pontua��o do Jogador em caso de ERRO;
	float acerto 			= 333.0;  // Incremento da pontua��o do Jogador em caso de ACERTO;

									// +++++++++++++++++++++++++++
									//    DECLARA��O DE FUNCOES  + 
									// +++++++++++++++++++++++++++

	  //=====================================================//
	 // Fun��o Centraliza String e Converte em Mai�sculas:  //
	//=====================================================//
	char strCentroMaiuscula(char nome[100], int tamHorizontalTela){
		
		int i = 0;
		char espacos[100];
		
		for ( i = 0 ; i < ((tamHorizontalTela - strlen(nome)) / 2) ; i++ ){
			espacos[i] = ' ';
		}
		
		strcat(espacos, nome);
		
		printf("%s\n", strupr(espacos));
	}
	
//############################################################################################################################
//############################################################################################################################
	
		// ***************************************************************************************************
		// *                               FUN��O ESTRUTURAL PARA PERGUNTAS                                  *
		// ***************************************************************************************************

	int pergunta(char nomeJogador, float pontos, float erro, float acerto, char Perg[], char alt_1, char alt_2, char alt_3, char alt_4){
		
		int RespUsuarioPerg = 0;
		
		printf ("================================================================================");
		printf ("                               ..:: JOGADOR ::..                               \n");
		printf ("%37s\n", nomeJogador);															//Fun��o Centraliza String e Converte em Mai�sculas
		printf ("          -------------------------------------------------------------        \n");
		printf ("                              ||  PONTUA��O  ||");
		printf ("\n");
		printf ("\t|| CR�DITOS = R$ %d    ||-|| ERRAR = R$ %d ||-|| ACERTAR = R$ %d ||\n", pontos, erro, acerto);        
		printf ("================================================================================");
		printf ("                                <<< PERGUNTA >>>                               \n");
		printf ("================================================================================");
		printf ("\n");
		printf ("\n");
		printf ("\n");
		//printf ("                                                                               ");
		printf ("%s\n", Perg);
		//printf ("                                                                               ");
		printf ("\n");
		printf ("\n");
		printf ("\n");
		printf ("================================================================================");
		printf ("                              <<< ALTERNATIVAS >>>                              ");
		printf ("================================================================================");
		printf ("\n");
		printf (" 1 - %s\n", alt_1);
		printf (" 2 - %s\n", alt_2);
		printf (" 3 - %s\n", alt_3);
		printf (" 4 - %s\n", alt_4);
		printf ("\n");
		printf ("================================================================================");
		printf ("================================================================================");
		printf ("\n");
		printf ("\n");
		
		printf ("Qual � a resposta CERTA? >>> ");
		scanf("%d", &RespUsuarioPerg);
		getchar();
		
		system("CLS");
		
		return RespUsuarioPerg;
	}	

//############################################################################################################################
//############################################################################################################################

					//=======================================================================
					// 			ATRIBUINDO PERGUNTAS - ALTERNATIVAS - RESPOSTAS				=
					//							>>> RODADA A <<<							=
					//=======================================================================

	char PergA1[200] = "QUAL PERSONAGEM DA TURMA DO CHAVES VIVE COBRANDO O ALUGUEL AO SEU MADRUGA?";
	char altA1_1[100] = "KIKO";
	char altA1_2[100] = "CHAVES";
	char altA1_3[100] = "SEU BARRIGA";
	char altA1_4[100] = "PROFESSOR GIRAFALES";
	int RespCertaPergA1 = 3;
	int pergSorteada_A1 = 0;	// 0 - DIZ-SE QUANDO A PERGUNTA AINDA N�O FOI SORTEADA.
	                    		// 1 - DIZ-SE QUANDO A PERGUNTA J� FOI SORTEADA, PARA QUE N�O SEJA REPETIDA, ALTERA-SE SEU VALOR DE 0 PARA 1.

	char PergA2[200] = "QUAL PA�S FIDEL CASTRO REVOLUCIONOU JUNTO COM CHE GUEVARA?";
	char altA2_1[100] = "JAMAICA";
	char altA2_2[100] = "CUBA";
	char altA2_3[100] = "EL SALVADOR";
	char altA2_4[100] = "M�XICO";
	int RespCertaPergA2 = 2;
	int pergSorteada_A2 = 0;
	
	char PergA3[200] = "QUAL A COR ASSOCIADA A GRUPOS ECOL�GICOS?";
	char altA3_1[100] = "PRETO";
	char altA3_2[100] = "VERMELHO";
	char altA3_3[100] = "AZUL";
	char altA3_4[100] = "VERDE";
	int RespCertaPergA3 = 4;
	int pergSorteada_A3 = 0;
	
	char PergA4[200] = "COM QUANTOS GRAUS CENT�GRADOS A �GUA FERVE?";
	char altA4_1[100] = "200";
	char altA4_2[100] = "100";
	char altA4_3[100] = "170";
	char altA4_4[100] = "220";
	int RespCertaPergA4 = 2;
	int pergSorteada_A4 = 0;
	
	char PergA5[200] = "QUANDO � COMEMORADO O DIA DA INDEPEND�NCIA DO BRASIL?";
	char altA5_1[100] = "21 DE ABRIL";
	char altA5_2[100] = "12 DE OUTUBRO";
	char altA5_3[100] = "7 DE SETEMBRO";
	char altA5_4[100] = "25 DE DEZEMBRO";
	int RespCertaPergA5 = 3;
	int pergSorteada_A5 = 0;
	
	char PergA6[200] = "QUAL O NOME DA �REA EM ROMA NA QUAL VIVE O PAPA?";
	char altA6_1[100] = "VENEZA";
	char altA6_2[100] = "VIT�RIA";
	char altA6_3[100] = "VANCOUVER";
	char altA6_4[100] = "VATICANO";
	int RespCertaPergA6 = 4;
	int pergSorteada_A6 = 0;
					
	char PergA7[200] = "QUE FERIADO � COMEMORADO NO DIA 1� DE MAIO?";
	char altA7_1[100] = "DIA DO AVIADOR";
	char altA7_2[100] = "DIA DO TRABALHO";
	char altA7_3[100] = "DIA DAS M�ES";
	char altA7_4[100] = "DIA DA BANDEIRA";
	int RespCertaPergA7 = 2;
	int pergSorteada_A7 = 0;
				
	char PergA8[200] = "QUEM INVENTOU O TELEFONE?";
	char altA8_1[100] = "GRAHAM BELL";
	char altA8_2[100] = "G. WASHINGTON";
	char altA8_3[100] = "TOMAS EDISON";
	char altA8_4[100] = "MARCONI";
	int RespCertaPergA8 = 1;
	int pergSorteada_A8 = 0;

	char PergA9[200] = "QUAL  PERSONAGEM DA TURMA DA M�NICA  TEM  CINCO FIOS DE CABELO?";
	char altA9_1[100] = "M�NICA";
	char altA9_2[100] = "CEBOLINHA";
	char altA9_3[100] = "CASC�O";
	char altA9_4[100] = "MAGALI";
	int RespCertaPergA9 = 2;
	int pergSorteada_A9 = 0;
		 			
	char PergA10[200] = "QUEM CRIOU OS PERSONAGENS PEDRINHO E NARIZINHO?";
	char altA10_1[100] = "M. DE SOUSA";
	char altA10_2[100] = "ZIRALDO";
	char altA10_3[100] = "M. LOBATO";
	char altA10_4[100] = "M. C. MACHADO";
	int RespCertaPergA10 = 3;
	int pergSorteada_A10 = 0;
				
	char PergA11[200] = "QUAL � O HOMEM MAIS RICO DO MUNDO ATUALMENTE?";
	char altA11_1[100] = "SULT�O DE BRUNEI";
	char altA11_2[100] = "AKIO MORITA";
	char altA11_3[100] = "BILL GATES";
	char altA11_4[100] = "O DONO DO MANA�RA SHOPPING";
	int RespCertaPergA11 = 3;
	int pergSorteada_A11 = 0;
				
	char PergA12[200] = "QUEM CRIOU OS PERSONAGENS DA TURMA DA M�NICA?";
	char altA12_1[100] = "MONTEIRO LOBATO";
	char altA12_2[100] = "JORGE AMADO";
	char altA12_3[100] = "MAUR�CIO DE SOUSA";
	char altA12_4[100] = "ANGELI";
	int RespCertaPergA12 = 3;
	int pergSorteada_A12 = 0;
	
	char PergA13[200] = "QUAL � A PEDRA PRECIOSA MAIS DURA ENCONTRADA NA NATUREZA?";
	char altA13_1[100] = "ESMERALDA";
	char altA13_2[100] = "RUBI";
	char altA13_3[100] = "P�ROLA";
	char altA13_4[100] = "DIAMANTE";
	int RespCertaPergA13 = 4;
	int pergSorteada_A13 = 0;
				
	char PergA14[200] = "QUE CANTOR AMERICANO FICOU CONHECIDO COMO 'O REI DO ROCK'?";
	char altA14_1[100] = "FRANK SINATRA";
	char altA14_2[100] = "LITTLE RICHARD";
	char altA14_3[100] = "ELVIS PRESLEY";
	char altA14_4[100] = "RICHIE VALENS";
	int RespCertaPergA14 = 3;
	int pergSorteada_A14 = 0;
	
	char PergA15[200] = "QUANTOS DIAS TEM UM ANO BISSEXTO?";
	char altA15_1[100] = "364";
	char altA15_2[100] = "365";
	char altA15_3[100] = "366";
	char altA15_4[100] = "367";
	int RespCertaPergA15 = 3;
	int pergSorteada_A15 = 0;
				
	char PergA16[200] = "QUAL � A CAPITAL DO CINEMA MUNDIAL?";
	char altA16_1[100] = "GRAMADO";
	char altA16_2[100] = "LAS VEGAS";
	char altA16_3[100] = "NEW ORLEANS";
	char altA16_4[100] = "HOLLYWOOD";
	int RespCertaPergA16 = 4;
	int pergSorteada_A16 = 0;
			
	char PergA17[200] = "QUE DIA � COMEMORADO A PROCLAMA��O DA REP�BLICA NO BRASIL?";
	char altA17_1[100] = "19 DE ABRIL";
	char altA17_2[100] = "15 DE OUTUBRO";
	char altA17_3[100] = "19 DE NOVEMBRO";
	char altA17_4[100] = "15 DE NOVEMBRO";
	int RespCertaPergA17 = 4;
	int pergSorteada_A17 = 0;
					
	char PergA18[200] = "DE QUANTO O BRASIL PERDEU DA FRAN�A NA FINAL DA COPA DE 98?";
	char altA18_1[100] = "2 X 0";
	char altA18_2[100] = "3 X 0";
	char altA18_3[100] = "4 X 0";
	char altA18_4[100] = "5 X 0";
	int RespCertaPergA18 = 2;
	int pergSorteada_A18 = 0;
	
	char PergA19[200] = "O QUE VEM DEPOIS DO VER�O E ANTES DO INVERNO?";
	char altA19_1[100] = "OUTONO";
	char altA19_2[100] = "PRIMAVERA";
	char altA19_3[100] = "INVERNO";
	char altA19_4[100] = "VER�O";
	int RespCertaPergA19 = 1;
	int pergSorteada_A19 = 0;
	
	char PergA20[200] = "VIOLONCELO � UM INSTRUMENTO DE:";
	char altA20_1[100] = "SOPRO";
	char altA20_2[100] = "CORDAS";
	char altA20_3[100] = "PERCUSS�O";
	char altA20_4[100] = "REPERCUSS�O";
	int RespCertaPergA20 = 2;
	int pergSorteada_A20 = 0;
				
	char PergA21[200] = "QUAL �REA DA MEDICINA QUE TRATA AS CRIAN�AS?";
	char altA21_1[100] = "GERIATRIA";
	char altA21_2[100] = "PEDIATRIA";
	char altA21_3[100] = "INFANTOLOGIA";
	char altA21_4[100] = "BIOLOGIA";
	int RespCertaPergA21 = 2;
	int pergSorteada_A21 = 0;
	
	char PergA22[200] = "O SAQU� � UMA BEBIDA ORIGIN�RIA DE QUE PA�S?";
	char altA22_1[100] = "ESPANHA";
	char altA22_2[100] = "JAP�O";
	char altA22_3[100] = "COR�IA DO SUL";
	char altA22_4[100] = "CHINA";
	int RespCertaPergA22 = 2;
	int pergSorteada_A22 = 0;
		 		
	char PergA23[200] = "UM ADULTO SADIO TEM QUANTOS DENTES NA BOCA?";
	char altA23_1[100] = "18 DENTES";
	char altA23_2[100] = "24 DENTES";
	char altA23_3[100] = "32 DENTES";
	char altA23_4[100] = "36 DENTES";
	int RespCertaPergA23 = 3;
	int pergSorteada_A23 = 0;
	
	char PergA24[200] = "TURMALINA � UMA ESP�CIE DE QU�?";
	char altA24_1[100] = "FLOR";
	char altA24_2[100] = "FRUTA";
	char altA24_3[100] = "PEDRA";
	char altA24_4[100] = "VERDURA";
	int RespCertaPergA24 = 3;
	int pergSorteada_A24 = 0;
	
	char PergA25[200] = "QUE TIPO DE INSTRUMENTO � O PIANO?";
	char altA25_1[100] = "SOPRO";
	char altA25_2[100] = "CORDA";
	char altA25_3[100] = "PERCUSS�O";
	char altA25_4[100] = "FOR�A";
	int RespCertaPergA25 = 2;
	int pergSorteada_A25 = 0;
	
	char lista_perguntasA[25] = { PergA1, PergA2, PergA3, PergA4, PergA5, PergA6, PergA7, PergA8, PergA9, PergA10, PergA11, PergA12, PergA13, PergA14, PergA15, PergA16, PergA17, PergA18, PergA19, PergA20, PergA21, PergA22, PergA23, PergA24, PergA25};

	char lista_altA[25][4] = {
				             {altA1_1, altA1_2, altA1_3, altA1_4},
				             {altA2_1, altA2_2, altA2_3, altA2_4},
				             {altA3_1, altA3_2, altA3_3, altA3_4},
				             {altA4_1, altA4_2, altA4_3, altA4_4},
				             {altA5_1, altA5_2, altA5_3, altA5_4},
				             {altA6_1, altA6_2, altA6_3, altA6_4},
				             {altA7_1, altA7_2, altA7_3, altA7_4},
				             {altA8_1, altA8_2, altA8_3, altA8_4},
				             {altA9_1, altA9_2, altA9_3, altA9_4}, 
				             {altA10_1, altA10_2, altA10_3, altA10_4},
				             {altA11_1, altA11_2, altA11_3, altA11_4},
				             {altA12_1, altA12_2, altA12_3, altA12_4},
				             {altA13_1, altA13_2, altA13_3, altA13_4},
				             {altA14_1, altA14_2, altA14_3, altA14_4},
				             {altA15_1, altA15_2, altA15_3, altA15_4},
				             {altA16_1, altA16_2, altA16_3, altA16_4},
				             {altA17_1, altA17_2, altA17_3, altA17_4},
				             {altA18_1, altA18_2, altA18_3, altA18_4},
				             {altA19_1, altA19_2, altA19_3, altA19_4},
				             {altA20_1, altA20_2, altA20_3, altA20_4},
				             {altA21_1, altA21_2, altA21_3, altA21_4},
				             {altA22_1, altA22_2, altA22_3, altA22_4},
				             {altA23_1, altA23_2, altA23_3, altA23_4},
				             {altA24_1, altA24_2, altA24_3, altA24_4},
				             {altA25_1, altA25_2, altA25_3, altA25_4}
	};
	
	int lista_respA[25] = {
				              RespCertaPergA1, RespCertaPergA2, RespCertaPergA3, RespCertaPergA4, RespCertaPergA5,
				              RespCertaPergA6, RespCertaPergA7, RespCertaPergA8, RespCertaPergA9, RespCertaPergA10,
				              RespCertaPergA11, RespCertaPergA12, RespCertaPergA13, RespCertaPergA14, RespCertaPergA15,
				              RespCertaPergA16, RespCertaPergA17, RespCertaPergA18, RespCertaPergA19, RespCertaPergA20,
				              RespCertaPergA21, RespCertaPergA22, RespCertaPergA23, RespCertaPergA24, RespCertaPergA25
	};
	
	int lista_sorteadasA[25] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

//############################################################################################################################
//############################################################################################################################
	
	//		...EM ELABORACAO...

	//		int* pergunta(nomeJogador, pontos, erro, acerto, lista_perguntasA[0], lista_altA[0][0], lista_altA[0][1], lista_altA[0][2], lista_altA[0][3]);

	
//############################################################################################################################
//############################################################################################################################


									// +++++++++++++++++++++++++++
									// TELA INICIAL DO PROGRAMA: + 
									// +++++++++++++++++++++++++++

	system("color 1F");
	
	printf ("================================================================================");
	printf ("\n");
	printf ("   ||                           ----------------                           || \n");
	printf ("   ||                          *** INTELECTUS ***                          || \n");
	printf ("   ||                           ----------------                           || \n");
	printf ("   ||                                                                      || \n");
	printf ("   ||                 ----------------------------------                   || \n");
	printf ("   ||                   ... PROVE QUE N�O �S BURRO ...                     || \n");
	printf ("   ||                                 ...                                  || \n");
	printf ("   ||                      DESAFIE SUA INTELIG�NCIA!                       || \n");
	printf ("   ||                       -----------------------                        || \n");
	printf ("\n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	printf("...Pressione ENTER para ACEITAR o DESAFIO! >>>");
	scanf("x");
	getchar();
	system("CLS");

//############################################################################################################################
//############################################################################################################################

									// ++++++++++++++++++++++++++++++
									// RECEBENDO O NOME DO JOGADOR: + 
									// ++++++++++++++++++++++++++++++

	system("color F0");
	
	printf ("================================================================================");
	printf ("\n");
	printf ("   ||                           ----------------                           || \n");
	printf ("   ||                          *** INTELECTUS ***                          || \n");
	printf ("   ||                           ----------------                           || \n");
	printf ("   ||                                                                      || \n");
	printf ("   ||                  ----------------------------------                  || \n");
	printf ("   ||                                                                      || \n");
	printf ("   ||           ... VAMOS CONHECER AGORA O NOSSO PARTICIPANTE ...          || \n");
	printf ("   ||                                                                      || \n");
	printf ("   ||                        ----------------------                        || \n");
	printf ("\n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
			
	printf("...DIGITE SEU NOME: >>> ");
	scanf("%[ -~]", &nomeJogador);
	getchar();
	system("CLS");

//############################################################################################################################
//############################################################################################################################

									// +++++++++++++++++++++++++++++++
									// IMPRIMINDO O NOME DO JOGADOR: + 
									// +++++++++++++++++++++++++++++++
	
	system("color 0F");
	
	printf ("================================================================================");
	printf ("   ||                           ----------------                           || \n");
	printf ("   ||                          *** INTELECTUS ***                          || \n");
	printf ("   ||                           ----------------                           || \n");
	printf ("   ||                                                                      || \n");
	printf ("   ||                 ----------------------------------                   || \n");
	printf ("   ||                               PARAB�NS                               || \n");
	strCentroMaiuscula(nomeJogador, 80);															//Fun��o Centraliza String e Converte em Mai�sculas
	printf ("   ||           ... SABEMOS AO MENOS QUE VOC� TEM CORAGEM! ...             || \n");
	printf ("   ||                       -----------------------                        || \n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	
	printf("...Pressione ENTER para SE ARRISCAR UM POUCO MAIS... >>> ");
	scanf("x");
	getchar();
	system("CLS");

//############################################################################################################################
//############################################################################################################################


        
//############################################################################################################################
//############################################################################################################################        

										// =================================== 
										//         TABELA DE PONTUA��O:      =
										//									 =
										//      ACERTO: RODADA A =   1.000   = 
										//      ACERTO: RODADA B =  10.000   = 
										//      ACERTO: RODADA C = 100.000   = 
										//      ACERTO: RODADA D = 1 MILH�O  = 
										//        ERRO: PONTOS / 4           =
										// ===================================

//############################################################################################################################
//############################################################################################################################

								// ======================================================
								// =                      SETOR							=
								// =					>>> A <<<  						=
								// =													=
								// ======================================================

						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// TELA DE ABERTURA: RODADA DE PERGUNTAS N�VEL >>> A <<<: + 
						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	printf ("================================================================================");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                           *** INTELECTUS ***                          || \n");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                                                                       || \n");
	printf ("  ||                  ----------------------------------                   || \n");
	printf ("  ||       VAMOS DAR IN�CIO A RODADA DE PERGUNTAS VALENDO R$ 1.000,00.     || \n");
	printf ("  ||                                  ...                                  || \n");
	printf ("  ||                           ... BOA SORTE! ...                          || \n");
	printf ("  ||                        -----------------------                        || \n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	
	printf("... Pressione ENTER para CONTINUAR... >>> ");
	scanf("x");
	getchar();
	system("CLS");
	
//############################################################################################################################
//############################################################################################################################
	
								// ======================================================
								// =                      SETOR							=
								// =					>>> B <<<  						=
								// =													=
								// ======================================================

						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// TELA DE ABERTURA: RODADA DE PERGUNTAS N�VEL >>> B <<<: + 
						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	printf ("================================================================================");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                           *** INTELECTUS ***                          || \n");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                                                                       || \n");
	printf ("  ||                  ----------------------------------                   || \n");
	printf ("  ||      VAMOS DAR IN�CIO A RODADA DE PERGUNTAS VALENDO R$ 10.000,00.     || \n");
	printf ("  ||                                  ...                                  || \n");
	printf ("  ||                           ... BOA SORTE! ...                          || \n");
	printf ("  ||                        -----------------------                        || \n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	
	printf("... Pressione ENTER para CONTINUAR... >>> ");
	scanf("x");
	getchar();
	system("CLS");

//############################################################################################################################
//############################################################################################################################

								// ======================================================
								// =                      SETOR							=
								// =					>>> C <<<  						=
								// =													=
								// ======================================================

						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// TELA DE ABERTURA: RODADA DE PERGUNTAS N�VEL >>> C <<<: + 
						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	printf ("================================================================================");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                           *** INTELECTUS ***                          || \n");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                                                                       || \n");
	printf ("  ||                  ----------------------------------                   || \n");
	printf ("  ||     VAMOS DAR IN�CIO A RODADA DE PERGUNTAS VALENDO R$ 100.000,00.     || \n");
	printf ("  ||                                  ...                                  || \n");
	printf ("  ||                           ... BOA SORTE! ...                          || \n");
	printf ("  ||                        -----------------------                        || \n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	
	printf("... Pressione ENTER para CONTINUAR... >>> ");
	scanf("x");
	getchar();
	system("CLS");

//############################################################################################################################
//############################################################################################################################
	
								// ======================================================
								// =                      SETOR							=
								// =					>>> D <<<  						=
								// =													=
								// ======================================================

						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// TELA DE ABERTURA: RODADA DE PERGUNTAS N�VEL >>> D <<<: + 
						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	printf ("================================================================================");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                           *** INTELECTUS ***                          || \n");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                                                                       || \n");
	printf ("  ||                  ----------------------------------                   || \n");
	printf ("  ||        VAMOS DAR IN�CIO A RODADA DE PERGUNTAS VALENDO 1 MILH�O!!!     || \n");
	printf ("  ||                                  ...                                  || \n");
	printf ("  ||                           ... BOA SORTE! ...                          || \n");
	printf ("  ||                        -----------------------                        || \n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	
	printf("... Pressione ENTER para CONTINUAR... >>> ");
	scanf("x");
	getchar();
	system("CLS");
	
	return 0;
}
