#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>		// Configurar a linguagem
#include <windows.h>	// Personalizar as cores do CMD

int main(){
	setlocale(LC_ALL, "portuguese"); 
	
	// +++++++++++++++++++++++++++
	//  DECLARA��O DE VARI�VEIS: + 
	// +++++++++++++++++++++++++++

	char nomeJogador[100] 	= " ";	// Receber� o nome do Jogador;
	int pontos 				= 0;	// Receber� a pontua��o do Jogador ao decorrer do jogo;
	int erro 				= 0;	// Decremento da pontua��o do Jogador em caso de ERRO;
	int acerto 				= 0;	// Incremento da pontua��o do Jogador em caso de ACERTO;
	int NumerosPergA 		= 0;
	int NumerosPergB 		= 0;
	int NumerosPergC 		= 0;
	int NumerosPergD 		= 0;

//############################################################################################################################
//############################################################################################################################

					//=======================================================================
					// 			ATRIBUINDO PERGUNTAS - ALTERNATIVAS - RESPOSTAS				=
					//							>>> RODADA A <<<							=
					//=======================================================================

	char PergA1[200] = "QUAL PERSONAGEM DA TURMA DO CHAVES VIVE COBRANDO O ALUGUEL AO SEU MADRUGA?";
	char altA1_1[100] = "KIKO";
	char altA1_2[100] = "CHAVES";
	char altA1_3[100] = "SEU BARRIGA";
	char altA1_4[100] = "PROFESSOR GIRAFALES";
	int RespCertaPergA1 = 3;
	int pergSorteada_A1 = 0;	// 0 - DIZ-SE QUANDO A PERGUNTA AINDA N�O FOI SORTEADA.
	                    		// 1 - DIZ-SE QUANDO A PERGUNTA J� FOI SORTEADA, PARA QUE N�O SEJA REPETIDA, ALTERA-SE SEU VALOR DE 0 PARA 1.
	
	char PergA2[200] = "QUAL PA�S FIDEL CASTRO REVOLUCIONOU JUNTO COM CHE GUEVARA?";
	char altA2_1[100] = "JAMAICA";
	char altA2_2[100] = "CUBA";
	char altA2_3[100] = "EL SALVADOR";
	char altA2_4[100] = "M�XICO";
	int RespCertaPergA2 = 2;
	int pergSorteada_A2 = 0;
	
	char PergA3[200] = "QUAL A COR ASSOCIADA A GRUPOS ECOL�GICOS?";
	char altA3_1[100] = "PRETO";
	char altA3_2[100] = "VERMELHO";
	char altA3_3[100] = "AZUL";
	char altA3_4[100] = "VERDE";
	int RespCertaPergA3 = 4;
	int pergSorteada_A3 = 0;
	
	char PergA4[200] = "COM QUANTOS GRAUS CENT�GRADOS A �GUA FERVE?";
	char altA4_1[100] = "200";
	char altA4_2[100] = "100";
	char altA4_3[100] = "170";
	char altA4_4[100] = "220";
	int RespCertaPergA4 = 2;
	int pergSorteada_A4 = 0;
	
	char PergA5[200] = "QUANDO � COMEMORADO O DIA DA INDEPEND�NCIA DO BRASIL?";
	char altA5_1[100] = "21 DE ABRIL";
	char altA5_2[100] = "12 DE OUTUBRO";
	char altA5_3[100] = "7 DE SETEMBRO";
	char altA5_4[100] = "25 DE DEZEMBRO";
	int RespCertaPergA5 = 3;
	int pergSorteada_A5 = 0;
	
	char PergA6[200] = "QUAL O NOME DA �REA EM ROMA NA QUAL VIVE O PAPA?";
	char altA6_1[100] = "VENEZA";
	char altA6_2[100] = "VIT�RIA";
	char altA6_3[100] = "VANCOUVER";
	char altA6_4[100] = "VATICANO";
	int RespCertaPergA6 = 4;
	int pergSorteada_A6 = 0;
					
	char PergA7[200] = "QUE FERIADO � COMEMORADO NO DIA 1� DE MAIO?";
	char altA7_1[100] = "DIA DO AVIADOR";
	char altA7_2[100] = "DIA DO TRABALHO";
	char altA7_3[100] = "DIA DAS M�ES";
	char altA7_4[100] = "DIA DA BANDEIRA";
	int RespCertaPergA7 = 2;
	int pergSorteada_A7 = 0;
				
	char PergA8[200] = "QUEM INVENTOU O TELEFONE?";
	char altA8_1[100] = "GRAHAM BELL";
	char altA8_2[100] = "G. WASHINGTON";
	char altA8_3[100] = "TOMAS EDISON";
	char altA8_4[100] = "MARCONI";
	int RespCertaPergA8 = 1;
	int pergSorteada_A8 = 0;
	
	char PergA9[200] = "QUAL  PERSONAGEM DA TURMA DA M�NICA  TEM  CINCO FIOS DE CABELO?";
	char altA9_1[100] = "M�NICA";
	char altA9_2[100] = "CEBOLINHA";
	char altA9_3[100] = "CASC�O";
	char altA9_4[100] = "MAGALI";
	int RespCertaPergA9 = 2;
	int pergSorteada_A9 = 0;
		 			
	char PergA10[200] = "QUEM CRIOU OS PERSONAGENS PEDRINHO E NARIZINHO?";
	char altA10_1[100] = "M. DE SOUSA";
	char altA10_2[100] = "ZIRALDO";
	char altA10_3[100] = "M. LOBATO";
	char altA10_4[100] = "M. C. MACHADO";
	int RespCertaPergA10 = 3;
	int pergSorteada_A10 = 0;
				
	char PergA11[200] = "QUAL � O HOMEM MAIS RICO DO MUNDO ATUALMENTE?";
	char altA11_1[100] = "SULT�O DE BRUNEI";
	char altA11_2[100] = "AKIO MORITA";
	char altA11_3[100] = "BILL GATES";
	char altA11_4[100] = "O DONO DO MANA�RA SHOPPING";
	int RespCertaPergA11 = 3;
	int pergSorteada_A11 = 0;
				
	char PergA12[200] = "QUEM CRIOU OS PERSONAGENS DA TURMA DA M�NICA?";
	char altA12_1[100] = "MONTEIRO LOBATO";
	char altA12_2[100] = "JORGE AMADO";
	char altA12_3[100] = "MAUR�CIO DE SOUSA";
	char altA12_4[100] = "ANGELI";
	int RespCertaPergA12 = 3;
	int pergSorteada_A12 = 0;
	
	char PergA13[200] = "QUAL � A PEDRA PRECIOSA MAIS DURA ENCONTRADA NA NATUREZA?";
	char altA13_1[100] = "ESMERALDA";
	char altA13_2[100] = "RUBI";
	char altA13_3[100] = "P�ROLA";
	char altA13_4[100] = "DIAMANTE";
	int RespCertaPergA13 = 4;
	int pergSorteada_A13 = 0;
				
	char PergA14[200] = "QUE CANTOR AMERICANO FICOU CONHECIDO COMO 'O REI DO ROCK'?";
	char altA14_1[100] = "FRANK SINATRA";
	char altA14_2[100] = "LITTLE RICHARD";
	char altA14_3[100] = "ELVIS PRESLEY";
	char altA14_4[100] = "RICHIE VALENS";
	int RespCertaPergA14 = 3;
	int pergSorteada_A14 = 0;
	
	char PergA15[200] = "QUANTOS DIAS TEM UM ANO BISSEXTO?";
	char altA15_1[100] = "364";
	char altA15_2[100] = "365";
	char altA15_3[100] = "366";
	char altA15_4[100] = "367";
	int RespCertaPergA15 = 3;
	int pergSorteada_A15 = 0;
				
	char PergA16[200] = "QUAL � A CAPITAL DO CINEMA MUNDIAL?";
	char altA16_1[100] = "GRAMADO";
	char altA16_2[100] = "LAS VEGAS";
	char altA16_3[100] = "NEW ORLEANS";
	char altA16_4[100] = "HOLLYWOOD";
	int RespCertaPergA16 = 4;
	int pergSorteada_A16 = 0;
			
	char PergA17[200] = "QUE DIA � COMEMORADO A PROCLAMA��O DA REP�BLICA NO BRASIL?";
	char altA17_1[100] = "19 DE ABRIL";
	char altA17_2[100] = "15 DE OUTUBRO";
	char altA17_3[100] = "19 DE NOVEMBRO";
	char altA17_4[100] = "15 DE NOVEMBRO";
	int RespCertaPergA17 = 4;
	int pergSorteada_A17 = 0;
					
	char PergA18[200] = "DE QUANTO O BRASIL PERDEU DA FRAN�A NA FINAL DA COPA DE 98?";
	char altA18_1[100] = "2 X 0";
	char altA18_2[100] = "3 X 0";
	char altA18_3[100] = "4 X 0";
	char altA18_4[100] = "5 X 0";
	int RespCertaPergA18 = 2;
	int pergSorteada_A18 = 0;
	
	char PergA19[200] = "O QUE VEM DEPOIS DO VER�O E ANTES DO INVERNO?";
	char altA19_1[100] = "OUTONO";
	char altA19_2[100] = "PRIMAVERA";
	char altA19_3[100] = "INVERNO";
	char altA19_4[100] = "VER�O";
	int RespCertaPergA19 = 1;
	int pergSorteada_A19 = 0;
	
	char PergA20[200] = "VIOLONCELO � UM INSTRUMENTO DE:";
	char altA20_1[100] = "SOPRO";
	char altA20_2[100] = "CORDAS";
	char altA20_3[100] = "PERCUSS�O";
	char altA20_4[100] = "REPERCUSS�O";
	int RespCertaPergA20 = 2;
	int pergSorteada_A20 = 0;
				
	char PergA21[200] = "QUAL �REA DA MEDICINA QUE TRATA AS CRIAN�AS?";
	char altA21_1[100] = "GERIATRIA";
	char altA21_2[100] = "PEDIATRIA";
	char altA21_3[100] = "INFANTOLOGIA";
	char altA21_4[100] = "BIOLOGIA";
	int RespCertaPergA21 = 2;
	int pergSorteada_A21 = 0;
	
	char PergA22[200] = "O SAQU� � UMA BEBIDA ORIGIN�RIA DE QUE PA�S?";
	char altA22_1[100] = "ESPANHA";
	char altA22_2[100] = "JAP�O";
	char altA22_3[100] = "COR�IA DO SUL";
	char altA22_4[100] = "CHINA";
	int RespCertaPergA22 = 2;
	int pergSorteada_A22 = 0;
		 		
	char PergA23[200] = "UM ADULTO SADIO TEM QUANTOS DENTES NA BOCA?";
	char altA23_1[100] = "18 DENTES";
	char altA23_2[100] = "24 DENTES";
	char altA23_3[100] = "32 DENTES";
	char altA23_4[100] = "36 DENTES";
	int RespCertaPergA23 = 3;
	int pergSorteada_A23 = 0;
	
	char PergA24[200] = "TURMALINA � UMA ESP�CIE DE QU�?";
	char altA24_1[100] = "FLOR";
	char altA24_2[100] = "FRUTA";
	char altA24_3[100] = "PEDRA";
	char altA24_4[100] = "VERDURA";
	int RespCertaPergA24 = 3;
	int pergSorteada_A24 = 0;
	
	char PergA25[200] = "QUE TIPO DE INSTRUMENTO � O PIANO?";
	char altA25_1[100] = "SOPRO";
	char altA25_2[100] = "CORDA";
	char altA25_3[100] = "PERCUSS�O";
	char altA25_4[100] = "FOR�A";
	int RespCertaPergA25 = 2;
	int pergSorteada_A25 = 0;
	
	char *lista_perguntasA[25] = { 
									PergA1, PergA2, PergA3, PergA4, PergA5, 
									PergA6, PergA7, PergA8, PergA9, PergA10, 
									PergA11, PergA12, PergA13, PergA14, PergA15,
									PergA16, PergA17, PergA18, PergA19, PergA20, 
									PergA21, PergA22, PergA23, PergA24, PergA25
								 };
	
	char *lista_altA[25][4] = {
					             {altA1_1, altA1_2, altA1_3, altA1_4},
					             {altA2_1, altA2_2, altA2_3, altA2_4},
					             {altA3_1, altA3_2, altA3_3, altA3_4},
					             {altA4_1, altA4_2, altA4_3, altA4_4},
					             {altA5_1, altA5_2, altA5_3, altA5_4},
					             {altA6_1, altA6_2, altA6_3, altA6_4},
					             {altA7_1, altA7_2, altA7_3, altA7_4},
					             {altA8_1, altA8_2, altA8_3, altA8_4},
					             {altA9_1, altA9_2, altA9_3, altA9_4}, 
					             {altA10_1, altA10_2, altA10_3, altA10_4},
					             {altA11_1, altA11_2, altA11_3, altA11_4},
					             {altA12_1, altA12_2, altA12_3, altA12_4},
					             {altA13_1, altA13_2, altA13_3, altA13_4},
					             {altA14_1, altA14_2, altA14_3, altA14_4},
					             {altA15_1, altA15_2, altA15_3, altA15_4},
					             {altA16_1, altA16_2, altA16_3, altA16_4},
					             {altA17_1, altA17_2, altA17_3, altA17_4},
					             {altA18_1, altA18_2, altA18_3, altA18_4},
					             {altA19_1, altA19_2, altA19_3, altA19_4},
					             {altA20_1, altA20_2, altA20_3, altA20_4},
					             {altA21_1, altA21_2, altA21_3, altA21_4},
					             {altA22_1, altA22_2, altA22_3, altA22_4},
					             {altA23_1, altA23_2, altA23_3, altA23_4},
					             {altA24_1, altA24_2, altA24_3, altA24_4},
					             {altA25_1, altA25_2, altA25_3, altA25_4}
	};
	
	int *lista_respA[25] = 	{
				              RespCertaPergA1, RespCertaPergA2, RespCertaPergA3, RespCertaPergA4, RespCertaPergA5,
				              RespCertaPergA6, RespCertaPergA7, RespCertaPergA8, RespCertaPergA9, RespCertaPergA10,
				              RespCertaPergA11, RespCertaPergA12, RespCertaPergA13, RespCertaPergA14, RespCertaPergA15,
				              RespCertaPergA16, RespCertaPergA17, RespCertaPergA18, RespCertaPergA19, RespCertaPergA20,
				              RespCertaPergA21, RespCertaPergA22, RespCertaPergA23, RespCertaPergA24, RespCertaPergA25
							};
	
	int lista_sorteadasA[25] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	

	
//############################################################################################################################
//############################################################################################################################

					//=======================================================================
					// 			ATRIBUINDO PERGUNTAS - ALTERNATIVAS - RESPOSTAS				=
					//							>>> RODADA B <<<							=
					//=======================================================================
					
	char pergB1[200] = "UMA PESSOA CLEPTOMAN�ACA �:";
	char altB1_1[100] = "COLECIONADORA";
	char altB1_2[100] = "DECORADORA";
	char altB1_3[100] = "M�DICA";
	char altB1_4[100] = "DOENTE";
	int respCertaPergB1 = 4;
	int pergSorteada_B1 = 0;
				
	char pergB2[200] = "DE ONDE ERA NATURAL O PINTOR PICASSO?";
	char altB2_1[100] = "ESPANHA";
	char altB2_2[100] = "HOLANDA";
	char altB2_3[100] = "FRAN�A";
	char altB2_4[100] = "B�LGICA";
	int respCertaPergB2 = 1;
	int pergSorteada_B2 = 0;
				
	char pergB3[200] = "QUE IMPERADOR TOCOU FOGO EM ROMA?";
	char altB3_1[100] = "C�SAR";
	char altB3_2[100] = "NERO";
	char altB3_3[100] = "BRUTUS";
	char altB3_4[100] = "CAL�GULA";
	int respCertaPergB3 = 2;
	int pergSorteada_B3 = 0;
				
	char pergB4[200] = "EM QUAL EST�DIO, PEL� MARCOU SEU MIL�SIMO GOL?";
	char altB4_1[100] = "MORUMBI";
	char altB4_2[100] = "PACAEMBU";
	char altB4_3[100] = "MARACAN�";
	char altB4_4[100] = "MINEIR�O";
	int respCertaPergB4 = 3;
	int pergSorteada_B4 = 0;
				
	char pergB5[200] = "O QUE � UM OBO� ?";
	char altB5_1[100] = "VULC�O";
	char altB5_2[100] = "COMIDA";
	char altB5_3[100] = "INSTRUMENTO";
	char altB5_4[100] = "TRIBO";
	int respCertaPergB5 = 3;
	int pergSorteada_B5 = 0;
				
	char pergB6[200] = "COMO ERAM CHAMADOS OS PILOTOS SUICIDAS DA SEGUNDA GUERRA?";
	char altB6_1[100] = "KAMIKAZES";
	char altB6_2[100] = "SASHIMIS";
	char altB6_3[100] = "HARA-KIRIS";
	char altB6_4[100] = "ARIGAT�S";
	int respCertaPergB6 = 1;
	int pergSorteada_B6 = 0;
				
	char pergB7[200] = "O COLISEU � UM MONUMENTO HIST�RICO DE QUE CIDADE EUROP�IA?";
	char altB7_1[100] = "PARIS";
	char altB7_2[100] = "COPENHAGUE";
	char altB7_3[100] = "ROMA";
	char altB7_4[100] = "LONDRES";
	int respCertaPergB7 = 3;
	int pergSorteada_B7 = 0;
				
	char pergB8[200] = "QUEM FOI ELEITO PRESIDENTE DA �FRICA DO SUL EM 1994?";
	char altB8_1[100] = "NELSON PIQUET";
	char altB8_2[100] = "N. MANDELA";
	char altB8_3[100] = "NELSON EDDY";
	char altB8_4[100] = "JOHN NELSON";
	int respCertaPergB8 = 2;
	int pergSorteada_B8 = 0;
				
	char pergB9[200] = "QUANTOS KILATES TEM O OURO PURO?";
	char altB9_1[100] = "18";
	char altB9_2[100] = "20";
	char altB9_3[100] = "24";
	char altB9_4[100] = "30";
	int respCertaPergB9 = 3;
	int pergSorteada_B9 = 0;
		 			
	char pergB10[200] = "QUAL A SIGLA DA ORGANIZA��O DAS NA��ES UNIDAS?";
	char altB10_1[100] = "ONU";
	char altB10_2[100] = "FMI";
	char altB10_3[100] = "CIA";
	char altB10_4[100] = "INPS";
	int respCertaPergB10 = 1;
	int pergSorteada_B10 = 0;
				
	char pergB11[200] = "QUAL A MOEDA OFICIAL DA ALEMANHA?";
	char altB11_1[100] = "LIRA";
	char altB11_2[100] = "MARCO";
	char altB11_3[100] = "PECETA";
	char altB11_4[100] = "IENE";
	int respCertaPergB11 = 2;
	int pergSorteada_B11 = 0;
	 			
	char pergB12[200] = "QUE PA�S EUROPEU TEM COMO ATRA��O A TOURADA ?";
	char altB12_1[100] = "ESPANHA";
	char altB12_2[100] = "IT�LIA";
	char altB12_3[100] = "FRAN�A";
	char altB12_4[100] = "ALEMANHA";
	int respCertaPergB12 = 1;
	int pergSorteada_B12 = 0;
	
	char pergB13[200] = "QUE HUMORISTA, FALECIDO EM 99, FOI BATERISTA DE RAUL SEIXAS?";
	char altB13_1[100] = "LILICO";
	char altB13_2[100] = "RONI C�CEGAS";
	char altB13_3[100] = "GRANDE OTELO";
	char altB13_4[100] = "MAZZAROPI";
	int respCertaPergB13 = 2;
	int pergSorteada_B13 = 0;
	 			
	char pergB14[200] = "O QU� OS FILATELISTAS COLECIONAM?";
	char altB14_1[100] = "QUADROS";
	char altB14_2[100] = "MOEDAS";
	char altB14_3[100] = "SELOS";
	char altB14_4[100] = "FIGURINHAS";
	int respCertaPergB14 = 3;
	int pergSorteada_B14 = 0;
	
	char pergB15[200] = "EM QUE CIDADE EST� LOCALIZADA A FAMOSA 'PRA�A VERMELHA'?";
	char altB15_1[100] = "MOSCOU";
	char altB15_2[100] = "BERLIM";
	char altB15_3[100] = "PARIS";
	char altB15_4[100] = "ROMA";
	int respCertaPergB15 = 1;
	int pergSorteada_B15 = 0;
				
	char pergB16[200] = "QUANDO COME�OU E TERMINOU A PRIMEIRA GUERRA MUNDIAL?";
	char altB16_1[100] = "1914-1918";
	char altB16_2[100] = "1902-1908";
	char altB16_3[100] = "1920-1930";
	char altB16_4[100] = "1912-1915";
	int respCertaPergB16 = 1;
	int pergSorteada_B16 = 0;
				
	char pergB17[200] = "QUEM INTRODUZIU O FUTEBOL NO BRASIL?";
	char altB17_1[100] = "PEL�";
	char altB17_2[100] = "JO�O HAVELANGE";
	char altB17_3[100] = "CHARLES MILLER";
	char altB17_4[100] = "PAULO MACHADO";
	int respCertaPergB17 = 3;
	int pergSorteada_B17 = 0;
				
	char pergB18[200] = "QUEM LEVA O SANGUE DO CORA��O PARA O CORPO?";
	char altB18_1[100] = "VEIAS";
	char altB18_2[100] = "M�SCULOS";
	char altB18_3[100] = "ART�RIAS";
	char altB18_4 = "OSSOS";
	int respCertaPergB18 = 3;
	int pergSorteada_B18 = 0;
	
	char pergB19[200] = "QUEM DISSE A FRASE: ' VIM, VI E VENCI ' NO ANO 47 ANTES DE CRISTO?";
	char altB19_1[100] = "J�LIO C�SAR";
	char altB19_2[100] = "CAL�GULA";
	char altB19_3[100] = "NERO";
	char altB19_4[100] = "MARCO ANTONIO";
	int respCertaPergB19 = 1;
	int pergSorteada_B19 = 0;
				
	char pergB20[200] = "QUAL � O PA�S QUE PARTICIPOU DE TODAS AS COPAS DO MUNDO DE FUTEBOL?";
	char altB20_1[100] = "IT�LIA";
	char altB20_2[100] = "URUGUAI";
	char altB20_3[100] = "ARGENTINA";
	char altB20_4[100] = "BRASIL";
	int respCertaPergB20 = 4;
	int pergSorteada_B20 = 0;
							
	char pergB21[200] = "QUAIS S�O OS NAIPES VERMELHOS DO BARALHO?";
	char altB21_1[100] = "OUROS E COPAS";
	char altB21_2[100] = "COPAS E PAUS";
	char altB21_3[100] = "PAUS E OUROS";
	char altB21_4[100] = "ESPADAS E PAUS";
	int respCertaPergB21 = 1;
	int pergSorteada_B21 = 0;
				
	char pergB22[200] = "QUAL O NOME VERDADEIRO DO BATMAN?";
	char altB22_1[100] = "BRUCE WAYNE";
	char altB22_2[100] = "BRUCE BANER";
	char altB22_3[100] = "CLARK KENT";
	char altB22_4[100] = "LEX LUTOR";
	int respCertaPergB22 = 1;
	int pergSorteada_B22 = 0;
					 		
	char pergB23[200] = "QUAL � A CAPITAL DO IRAQUE?";
	char altB23_1[100] = "BEL�M";
	char altB23_2[100] = "BAGD�";
	char altB23_3[100] = "BEIRUTE";
	char altB23_4[100] = "BUDAPESTE";
	int respCertaPergB23 = 2;
	int pergSorteada_B23 = 0;
				
	char pergB24[200] = "EM QUE PA�S FICA A GRANDE MURALHA CONSTRU�DA PELO HOMEM ?";
	char altB24_1[100] = "JAP�O";
	char altB24_2[100] = "CHINA";
	char altB24_3[100] = "AFEGANIST�O";
	char altB24_4[100] = "�NDIA";
	int respCertaPergB24 = 2;
	int pergSorteada_B24 = 0;
				
	char pergB25[200] = "EM QUAL CIDADE DO JAP�O FOI LAN�ADA A PRIMEIRA BOMBA AT�MICA?";
	char altB25_1[100] = "T�KIO";
	char altB25_2[100] = "NAGASAKI";
	char altB25_3[100] = "OSAKA";
	char altB25_4[100] = "HIROSHIMA";
	int respCertaPergB25 = 4;
	int pergSorteada_B25 = 0;
	
	char *lista_perguntasB[25] = {
									pergB1, pergB2, pergB3, pergB4, pergB5,
				                    pergB6, pergB7, pergB8, pergB9, pergB10,
				                    pergB11, pergB12, pergB13, pergB14, pergB15,
				                    pergB16, pergB17, pergB18, pergB19, pergB20,
				                    pergB21, pergB22, pergB23, pergB24, pergB25
								 };
	
	char *lista_altB[25][4] =	{
									{altB1_1, altB1_2, altB1_3, altB1_4},
									{altB2_1, altB2_2, altB2_3, altB2_4},
									{altB3_1, altB3_2, altB3_3, altB3_4},
									{altB4_1, altB4_2, altB4_3, altB4_4},
									{altB5_1, altB5_2, altB5_3, altB5_4},
									{altB6_1, altB6_2, altB6_3, altB6_4},
									{altB7_1, altB7_2, altB7_3, altB7_4},
									{altB8_1, altB8_2, altB8_3, altB8_4},
									{altB9_1, altB9_2, altB9_3, altB9_4}, 
									{altB10_1, altB10_2, altB10_3, altB10_4},
									{altB11_1, altB11_2, altB11_3, altB11_4},
									{altB12_1, altB12_2, altB12_3, altB12_4},
									{altB13_1, altB13_2, altB13_3, altB13_4},
									{altB14_1, altB14_2, altB14_3, altB14_4},
									{altB15_1, altB15_2, altB15_3, altB15_4},
									{altB16_1, altB16_2, altB16_3, altB16_4},
									{altB17_1, altB17_2, altB17_3, altB17_4},
									{altB18_1, altB18_2, altB18_3, altB18_4},
									{altB19_1, altB19_2, altB19_3, altB19_4},
									{altB20_1, altB20_2, altB20_3, altB20_4},
									{altB21_1, altB21_2, altB21_3, altB21_4},
									{altB22_1, altB22_2, altB22_3, altB22_4},
									{altB23_1, altB23_2, altB23_3, altB23_4},
									{altB24_1, altB24_2, altB24_3, altB24_4},
									{altB25_1, altB25_2, altB25_3, altB25_4}
								};
	
	int *lista_respB[25] = {
			              respCertaPergB1, respCertaPergB2, respCertaPergB3, respCertaPergB4, respCertaPergB5,
			              respCertaPergB6, respCertaPergB7, respCertaPergB8, respCertaPergB9, respCertaPergB10,
			              respCertaPergB11, respCertaPergB12, respCertaPergB13, respCertaPergB14, respCertaPergB15,
			              respCertaPergB16, respCertaPergB17, respCertaPergB18, respCertaPergB19, respCertaPergB20,
			              respCertaPergB21, respCertaPergB22, respCertaPergB23, respCertaPergB24, respCertaPergB25
						};
	
	int lista_sorteadasB[25] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	// FIM B

//############################################################################################################################
//############################################################################################################################

					//=======================================================================
					// 			ATRIBUINDO PERGUNTAS - ALTERNATIVAS - RESPOSTAS				=
					//							>>> RODADA C <<<							=
					//=======================================================================

	char PergC1[200] = "QUEM COMP�S O CONCERTO 'AS QUATROS ESTA��ES'?";
	char altC1_1[100] = "ANTONIO VIVALDI";
	char altC1_2[100] = "ANTONIO CARLOS JOBIM";
	char altC1_3[100] = "RICHARD WAGNER";
	char altC1_4[100] = "RICHARD DREYFUSS";
	int RespCertaPergC1 = 1;
	int pergSorteada_C1 = 0;
			
	char PergC2[200] = "QUEM � O AUTOR DO MANIFESTO COMUNISTA ?";
	char altC2_1[100] = "LENIN";
	char altC2_2[100] = "GORBATCHOV";
	char altC2_3[100] = "KARL MARX";
	char altC2_4[100] = "ALLAN KARDEC";
	int RespCertaPergC2 = 3;
	int pergSorteada_C2 = 0;
				
	char PergC3[200] = "QUAL DESSES PA�SES TEM A CAPITAL COM O MESMO NOME?";
	char altC3_1[100] = "IRAQUE";
	char altC3_2[100] = "LUXEMBURGO";
	char altC3_3[100] = "IUGOSL�VIA";
	char altC3_4[100] = "JORD�NIA";
	int RespCertaPergC3 = 2;
	int pergSorteada_C3 = 0;
				
	char PergC4[200] = "QUAL FLOR � O S�MBOLO NACIONAL DO EGITO E FOI CONSIDERADA SAGRADA NA ANTIGUIDADE?";
	char altC4_1[100] = "ROSA";
	char altC4_2[100] = "L�RIO";
	char altC4_3[100] = "JASMIM";
	char altC4_4[100] = "LOTUS";
	int RespCertaPergC4 = 4;
	int pergSorteada_C4 = 0;
				
	char PergC5[200] = "EM QUE PA�S FOI REALIZADA A PRIMEIRA COPA DO MUNDO DE FUTEBOL?";
	char altC5_1[100] = "PORTUGAL";
	char altC5_2[100] = "URUGUAI";
	char altC5_3[100] = "INGLATERRA";
	char altC5_4[100] = "IT�LIA";
	int RespCertaPergC5 = 2;
	int pergSorteada_C5 = 0;
				
	char PergC6[200] = "QUAL INSTRUMENTO MEDE A ALTITUDE DO SOL E OUTROS CORPOS CELESTES?";
	char altC6_1[100] = "SONAR";
	char altC6_2[100] = "RADAR";
	char altC6_3[100] = "SEXTANTE";
	char altC6_4[100] = "OD�METRO";
	int RespCertaPergC6 = 3;
	int pergSorteada_C6 = 0;
				
	char PergC7[200] = "COM QUANTOS P�ES JESUS CRISTO ALIMENTOU OS 5000?";
	char altC7_1[100] = "DOIS";
	char altC7_2[100] = "TR�S";
	char altC7_3[100] = "QUATRO";
	char altC7_4[100] = "CINCO";
	int RespCertaPergC7 = 4;
	int pergSorteada_C7 = 0;
		 		
	char PergC8[200] = "QUE  PA�S  DA  EUROPA  � CONHECIDO  COMO  PA�SES BAIXOS?";
	char altC8_1[100] = "HOLANDA";
	char altC8_2[100] = "�USTRIA";
	char altC8_3[100] = "B�LGICA";
	char altC8_4[100] = "HUNGRIA";
	int RespCertaPergC8 = 1;
	int pergSorteada_C8 = 0;
				
	char PergC9[200] = "O QUE � ORNITORRINCO?";
	char altC9_1[100] = "VULC�O";
	char altC9_2[100] = "RIO";
	char altC9_3[100] = "LEGUME";
	char altC9_4[100] = "ANIMAL";
	int RespCertaPergC9 = 4;
	int pergSorteada_C9 = 0;
				
	char PergC10[200] = "QUE ANIMAL MITOL�GICO RENASCIA DAS PR�PRIAS CINZAS?";
	char altC10_1[100] = "UM LE�O";
	char altC10_2[100] = "UMA AVE";
	char altC10_3[100] = "UM CAVALO";
	char altC10_4[100] = "UMA SERPENTE";
	int RespCertaPergC10 = 2;
	int pergSorteada_C10 = 0;
	 			
	char PergC11[200] = "QUAL CIDADE O ROBOCOP COMBATE O CRIME?";
	char altC11_1[100] = "NOVA YORK";
	char altC11_2[100] = "DETROIT";
	char altC11_3[100] = "LOS ANGELES";
	char altC11_4[100] = "SAN FRANCISCO";
	int RespCertaPergC11 = 2;
	int pergSorteada_C11 = 0;
				
	char PergC12[200] = "EM QUE ;ANO O BRASIL CONQUISTOU SUA PRIMEIRA COPA DO MUNDO ?";
	char altC12_1[100] = "1962";
	char altC12_2[100] = "1958";
	char altC12_3[100] = "1970";
	char altC12_4[100] = "1994";
	int RespCertaPergC12 = 2;
	int pergSorteada_C12 = 0;
	
	char PergC13[200] = "EM QUAL ESTADO BRASILEIRO ACONTECEU A GUERRA DE CANUDOS ?";
	char altC13_1[100] = "RIO DE JANEIRO";
	char altC13_2[100] = "BAHIA";
	char altC13_3[100] = "PERNAMBUCO";
	char altC13_4[100] = "CEAR�";
	int RespCertaPergC13 = 2;
	int pergSorteada_C13 = 0;
				
	char PergC14[200] = "QUEM LIDEROU A REVOLU��O CONTRA A DITADURA DE FULGENCIO BATISTA EM 1959?";
	char altC14_1[100] = "CHE GUEVARA";
	char altC14_2[100] = "FIDEL CASTRO";
	char altC14_3[100] = "JUAN D. PER�N";
	char altC14_4[100] = "GENERAL PINOCHET";
	int RespCertaPergC14 = 2;
	int pergSorteada_C14 = 0;
	
	char PergC15[200] = "EM QUE ANO O MURO DE BERLIM FOI DERRUBADO ?";
	char altC15_1[100] = "1975";
	char altC15_2[100] = "1984";
	char altC15_3[100] = "1990";
	char altC15_4[100] = "1989";
	int RespCertaPergC15 = 4;
	int pergSorteada_C15 = 0;
				
	char PergC16[200] = "QUEM TEM A INCUMB�NCIA DE ESCOLHER O NOSSO PAPA ?";
	char altC16_1[100] = "C�NEGOS";
	char altC16_2[100] = "BISPOS";
	char altC16_3[100] = "ARCEBISPOS";
	char altC16_4[100] = "CARDEAIS";
	int RespCertaPergC16 = 4;
	int pergSorteada_C16 = 0;
				
	char PergC17[200] = "QUEM ERA O DEUS SUPREMO NA ANTIGA RELIGI�O GREGA?";
	char altC17_1[100] = "MERC�RIO";
	char altC17_2[100] = "ZEUS";
	char altC17_3[100] = "APOLO";
	char altC17_4[100] = "NETUNO";
	int RespCertaPergC17 = 2;
	int pergSorteada_C17 = 0;
				
	char PergC18[200] = "QUAL ERA O DEUS ROMANO DA GERRA?";
	char altC18_1[100] = "MARTE";
	char altC18_2[100] = "J�PITER";
	char altC18_3[100] = "HADES";
	char altC18_4[100] = "NETUNO";
	int RespCertaPergC18 = 1;
	int pergSorteada_C18 = 0;
	
	char PergC19[200] = "EM QUE ASSUNTO UM EN�LOGO � ESPECIALISTA?";
	char altC19_1[100] = "MOEDAS";
	char altC19_2[100] = "VINHOS";
	char altC19_3[100] = "LIVROS";
	char altC19_4[100] = "�NDIOS";
	int RespCertaPergC19 = 2;
	int pergSorteada_C19 = 0;
				
	char PergC20[200] = "COMO REPRESENTAMOS EM ALGARISMOS ROMANOS O N�MERO 500 ?";
	char altC20_1[100] = "C";
	char altC20_2[100] = "M";
	char altC20_3[100] = "D";
	char altC20_4[100] = "L";
	int RespCertaPergC20 = 3;
	int pergSorteada_C20 = 0;
							
	char PergC21[200] = "QUINGENT�SIMO CORRESPONDE A QUAL N�MERO ?";
	char altC21_1[100] = "500";
	char altC21_2[100] = "5000";
	char altC21_3[100] = "50";
	char altC21_4[100] = "5";
	int RespCertaPergC21 = 1;
	int pergSorteada_C21 = 0;
				
	char PergC22[200] = "QUAL A COBRA DO BRASIL SE ALIMENTA DE OUTRA?";
	char altC22_1[100] = "MU�URANA";
	char altC22_2[100] = "CORAL";
	char altC22_3[100] = "CASCAVEL";
	char altC22_4[100] = "JIB�IA";
	int RespCertaPergC22 = 1;
	int pergSorteada_C22 = 0;
		 		
	char PergC23[200] = "EM UMA ARMADURA QUAL PARTE DO CORPO � PROTEGIDO PELO GUANTE?";
	char altC23_1[100] = "PEITO";
	char altC23_2[100] = "M�OS";
	char altC23_3[100] = "ROSTO";
	char altC23_4[100] = "ABDOME";
	int RespCertaPergC23 = 2;
	int pergSorteada_C23 = 0;
				
	char PergC24[200] = "MEDUSAS SIFON�FORAS E CTEN�FORAS S�O CATEGORIAS DE QU�?";
	char altC24_1[100] = "PLANTAS";
	char altC24_2[100] = "FLORES";
	char altC24_3[100] = "MAM�FEROS";
	char altC24_4[100] = "�GUAS-VIVAS";
	int RespCertaPergC24 = 4;
	int pergSorteada_C24 = 0;
		 		
	char PergC25[200] = "QUAL A CI�NCIA QUE ESTUDA A ORIGEM E DESENVOLVIMENTO DAS PALAVRAS?";
	char altC25_1[100] = "ENDOSCOPIA";
	char altC25_2[100] = "GRAFOLOGIA";
	char altC25_3[100] = "RADIOGRAFIA";
	char altC25_4[100] = "ETIMOLOGIA";
	int RespCertaPergC25 = 4;
	int pergSorteada_C25 = 0;
	
	char *lista_perguntasC[25] = {
								PergC1, PergC2, PergC3, PergC4, PergC5,
			                    PergC6, PergC7, PergC8, PergC9, PergC10,
			                    PergC11, PergC12, PergC13, PergC14, PergC15,
			                    PergC16, PergC17, PergC18, PergC19, PergC20,
			                    PergC21, PergC22, PergC23, PergC24, PergC25
								 };
	                    
	char *lista_altC[25][4] =	{
					             {altC1_1, altC1_2, altC1_3, altC1_4},
					             {altC2_1, altC2_2, altC2_3, altC2_4},
					             {altC3_1, altC3_2, altC3_3, altC3_4},
					             {altC4_1, altC4_2, altC4_3, altC4_4},
					             {altC5_1, altC5_2, altC5_3, altC5_4},
					             {altC6_1, altC6_2, altC6_3, altC6_4},
					             {altC7_1, altC7_2, altC7_3, altC7_4},
					             {altC8_1, altC8_2, altC8_3, altC8_4},
					             {altC9_1, altC9_2, altC9_3, altC9_4}, 
					             {altC10_1, altC10_2, altC10_3, altC10_4},
					             {altC11_1, altC11_2, altC11_3, altC11_4},
					             {altC12_1, altC12_2, altC12_3, altC12_4},
					             {altC13_1, altC13_2, altC13_3, altC13_4},
					             {altC14_1, altC14_2, altC14_3, altC14_4},
					             {altC15_1, altC15_2, altC15_3, altC15_4},
					             {altC16_1, altC16_2, altC16_3, altC16_4},
					             {altC17_1, altC17_2, altC17_3, altC17_4},
					             {altC18_1, altC18_2, altC18_3, altC18_4},
					             {altC19_1, altC19_2, altC19_3, altC19_4},
					             {altC20_1, altC20_2, altC20_3, altC20_4},
					             {altC21_1, altC21_2, altC21_3, altC21_4},
					             {altC22_1, altC22_2, altC22_3, altC22_4},
					             {altC23_1, altC23_2, altC23_3, altC23_4},
					             {altC24_1, altC24_2, altC24_3, altC24_4},
					             {altC25_1, altC25_2, altC25_3, altC25_4}
								};
	
	int *lista_respC[25] = {
				              RespCertaPergC1, RespCertaPergC2, RespCertaPergC3, RespCertaPergC4, RespCertaPergC5,
				              RespCertaPergC6, RespCertaPergC7, RespCertaPergC8, RespCertaPergC9, RespCertaPergC10,
				              RespCertaPergC11, RespCertaPergC12, RespCertaPergC13, RespCertaPergC14, RespCertaPergC15,
				              RespCertaPergC16, RespCertaPergC17, RespCertaPergC18, RespCertaPergC19, RespCertaPergC20,
				              RespCertaPergC21, RespCertaPergC22, RespCertaPergC23, RespCertaPergC24, RespCertaPergC25
						};
	
	int lista_sorteadasC[25] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	// FIM C

//############################################################################################################################
//############################################################################################################################

					//=======================================================================
					// 			ATRIBUINDO PERGUNTAS - ALTERNATIVAS - RESPOSTAS				=
					//							>>> RODADA D <<<							=
					//=======================================================================

	char PergD1[200] = "QUEM TEM MEDO DE C�ES SOFRE DE:";
	char altD1_1[100] = "CINOFOBIA";
	char altD1_2[100] = "CANILFOBIA";
	char altD1_3[100] = "C�OFOBIA";
	char altD1_4[100] = "CATFOBIA";
	int RespCertaPergD1 = 1;	
	int pergSorteada_D1 = 0;
				
	char PergD2[200] = "O QUE SIGNIFICA DIUTURNO?";
	char altD2_1[100] = "QUE VIVE DE DIA E DE NOITE";
	char altD2_2[100] = "QUE MORRE CEDO";
	char altD2_3[100] = "QUE VIVE MUITO TEMPO";
	char altD2_4[100] = "QUE VIVE DE DIA";
	int RespCertaPergD2 = 3;
	int pergSorteada_D2 = 0;
				
	char PergD3[200] = "QUE NOME RECEBE A ARTE DE GRAVAR EM PEDRAS PRECIOSAS?";
	char altD3_1[100] = "GL�PTICA";
	char altD3_2[100] = "FORMARTE";
	char altD3_3[100] = "CL�PTICA";
	char altD3_4[100] = "GRAFITA";
	int RespCertaPergD3 = 1;	
	int pergSorteada_D3 = 0;
				
	char PergD4[200] = "QUE NOME RECEBE A CONTRADI��O ENTRE DUAS LEIS?";
	char altD4_1[100] = "ANTINOMIA";
	char altD4_2[100] = "AUT�NOMO";
	char altD4_3[100] = "ANT�NIMO";
	char altD4_4[100] = "PAR�DIA";
	int RespCertaPergD4 = 1;	
	int pergSorteada_D4 = 0;
				
	char PergD5[200] = "QUEM DISSE: 'S� SEI QUE NADA SEI'?";
	char altD5_1[100] = "FREUD";
	char altD5_2[100] = "S�CRATES";
	char altD5_3[100] = "PLAT�O";
	char altD5_4[100] = "ARIST�TELES";
	int RespCertaPergD5 = 2;
	int pergSorteada_D5 = 0;
				
	char PergD6[200] = "O D�NAMO TRANSFORMA A ENERGIA MEC�NICA EM:";
	char altD6_1[100] = "NATURAL";
	char altD6_2[100] = "HUMANA";
	char altD6_3[100] = "EL�TRICA";
	char altD6_4[100] = "CIN�TICA";
	int RespCertaPergD6 = 3;	
	int pergSorteada_D6 = 0;
				
	char PergD7[200] = "SEGUNDO A B�BLIA, QUAL APOST�LO ERA ENCARREGADO DE CUIDAR DO DINHEIRO DO GRUPO?";
	char altD7_1[100] = "JO�O";
	char altD7_2[100] = "MATEUS";
	char altD7_3[100] = "JUDAS";
	char altD7_4[100] = "PEDRO";
	int RespCertaPergD7 = 3;	
	int pergSorteada_D7 = 0;
				
	char PergD8[200] = "TAUMATURGO � AQUELE QUE:";
	char altD8_1[100] = "N�O TRABALHA";
	char altD8_2[100] = "TRABALHA DE DIA";
	char altD8_3[100] = "FAZ MILAGRES";
	char altD8_4[100] = "TRABALHA MUITO";
	int RespCertaPergD8 = 3;
	int pergSorteada_D8 = 0;
				
	char PergD9[200] = "QUEM COMP�S A �PERA 'CARMEN'?";
	char altD9_1[100] = "MOZART";
	char altD9_2[100] = "GIUSEPPE VERDI";
	char altD9_3[100] = "GEORGES BIZET";
	char altD9_4[100] = "MAURICE RAVEL";
	int RespCertaPergD9 = 3;
	int pergSorteada_D9 = 0;
		 			
	char PergD10[200] = "POR ONDE AS BORBOLETAS SE ALIMENTAM?";
	char altD10_1[100] = "PROB�SCIDE";
	char altD10_2[100] = "ASAS";
	char altD10_3[100] = "OLHOS";
	char altD10_4[100] = "ANTENAS";
	int RespCertaPergD10 = 1;		
	int pergSorteada_D10 = 0;
				
	char PergD11[200] = "OS CURSOS FLUVIAIS TIGRE E EUFRATES EST�O LOCALIZADOS NA:";
	char altD11_1[100] = "AM�RICA DO NORTE";
	char altD11_2[100] = "AM�RICA CENTRAL";
	char altD11_3[100] = "AM�RICA DO SUL";
	char altD11_4[100] = "�SIA OCIDENTAL";
	int RespCertaPergD11 = 4;
	int pergSorteada_D11 = 0;
	
	char PergD12[200] = "QUAIS S�O AS CORES DA BANDEIRA DO PAQUIST�O?";
	char altD12_1[100] = "AZUL E LARANJA";
	char altD12_2[100] = "BRANCO E VERDE";
	char altD12_3[100] = "VERMELHO E VERDE";
	char altD12_4[100] = "AMARELO E AZUL";
	int RespCertaPergD12 = 2;
	int pergSorteada_D12 = 0;
	
	char PergD13[200] = "QUEM TEM OLHOS NEGROS � CHAMADO DE QU�?";
	char altD13_1[100] = "MELANITA";
	char altD13_2[100] = "MELANOPE";
	char altD13_3[100] = "MELAN�TICO";
	char altD13_4[100] = "MELAN�RIA";
	int RespCertaPergD13 = 2;	
	int pergSorteada_D13 = 0;
				
	char PergD14[200] = "QUAL DESSES FIL�SOFOS N�O � CONSIDERADO ILUMINISTA?";
	char altD14_1[100] = "ROUSSEAU";
	char altD14_2[100] = "BAKUNIN";
	char altD14_3[100] = "MONTESQUIEU";
	char altD14_4[100] = "VOLTAIRE";
	int RespCertaPergD14 = 2;
	int pergSorteada_D14 = 0;
	
	char PergD15[200] = "QUEM FOI TALES DE MILETO?";
	char altD15_1[100] = "PINTOR";
	char altD15_2[100] = "FIL�SOFO";
	char altD15_3[100] = "CANTOR";
	char altD15_4[100] = "IMPERADOR";
	int RespCertaPergD15 = 2;	
	int pergSorteada_D15 = 0;
				
	char PergD16[200] = "ONDE NASCEU O POETA BRASILEIRO GON�ALVES DIAS?";
	char altD16_1[100] = "MARANH�O";
	char altD16_2[100] = "S�O PAULO";
	char altD16_3[100] = "RIO DE JANEIRO";
	char altD16_4[100] = "MINAS GERAIS";
	int RespCertaPergD16 = 1;
	int pergSorteada_D16 = 0;
				
	char PergD17[200] = "O QUE SIGNIFICA EULALIA?";
	char altD17_1[100] = "BAGUN�A";
	char altD17_2[100] = "DOEN�A";
	char altD17_3[100] = "BOA DIC��O";
	char altD17_4[100] = "EUFORIA";
	int RespCertaPergD17 = 3;		
	int pergSorteada_D17 = 0;
				
	char PergD18[200] = "TIA EM QUARTO GRAU �:";
	char altD18_1[100] = "TETRAZINA";
	char altD18_2[100] = "ARAMITA";
	char altD18_3[100] = "ADIT�CIA";
	char altD18_4[100] = "AB�MITA";
	int RespCertaPergD18 = 4;		
	int pergSorteada_D18 = 0;
	
	char PergD19[200] = "QUAL DESTES N�O � UM ELEMENTO QU�MICO?";
	char altD19_1[100] = "TEL�RIO";
	char altD19_2[100] = "PERI�LIO";
	char altD19_3[100] = "VAN�DIO";
	char altD19_4[100] = "ESTR�NCIO";
	int RespCertaPergD19 = 2;	
	int pergSorteada_D19 = 0;
				
	char PergD20[200] = "QUEM DESCOBRIU OS RAIOS X?";
	char altD20_1[100] = "GALILEU";
	char altD20_2[100] = "THOMAS EDISON";
	char altD20_3[100] = "GRAHAM BELL";
	char altD20_4[100] = "WILHELM R�NTGEN";
	int RespCertaPergD20 = 4;
	int pergSorteada_D20 = 0;
							
	char PergD21[200] = "A GR�CIA FICA EM QUAL REGI�O DA EUROPA?";
	char altD21_1[100] = "SETENTRIONAL";
	char altD21_2[100] = "OCIDENTAL";
	char altD21_3[100] = "ORIENTAL";
	char altD21_4[100] = "MERIDIONAL";
	int RespCertaPergD21 = 4;	
	int pergSorteada_D21 = 0;
				
	char PergD22[200] = "QUAL O NOME DO MOVIMENTO FILOS�FICO SURGIDO NO S�CULO XVIII QUE ERA BASEADO NO USO DA RAZ�O?";
	char altD22_1[100] = "ILUMINISMO";
	char altD22_2[100] = "PROTESTANTISMO";
	char altD22_3[100] = "CALVINISMO";
	char altD22_4[100] = "CATOLICISMO";
	int RespCertaPergD22 = 1;	
	int pergSorteada_D22 = 0;
					 		
	char PergD23[200] = "EM QUAL D�CADA OCORREU A REVOLU��O CUBANA?";
	char altD23_1[100] = "1940";
	char altD23_2[100] = "1970";
	char altD23_3[100] = "1950";
	char altD23_4[100] = "1960";
	int RespCertaPergD23 = 3;		
	int pergSorteada_D23 = 0;
				
	char PergD24[200] = "EM QUE ANO MORREU ADOLF HITLER?";
	char altD24_1[100] = "1945";
	char altD24_2[100] = "1939";
	char altD24_3[100] = "1933";
	char altD24_4[100] = "1960";
	int RespCertaPergD24 = 1;
	int pergSorteada_D24 = 0;
				
	char PergD25[200] = "A DEUSA DA SABEDORIA ERA:";
	char altD25_1[100] = "ATENA";
	char altD25_2[100] = "HEBE";
	char altD25_3[100] = "AFRODITE";
	char altD25_4[100] = "ANT�GONA";
	int RespCertaPergD25 = 1;
	int pergSorteada_D25 = 0;
	
	char *lista_perguntasD[25] = {
									PergD1, PergD2, PergD3, PergD4, PergD5,
				                    PergD6, PergD7, PergD8, PergD9, PergD10,
				                    PergD11, PergD12, PergD13, PergD14, PergD15,
				                    PergD16, PergD17, PergD18, PergD19, PergD20,
				                    PergD21, PergD22, PergD23, PergD24, PergD25
							 	 };
						
	char *lista_altD[25][4] = {
					             {altD1_1, altD1_2, altD1_3, altD1_4},
					             {altD2_1, altD2_2, altD2_3, altD2_4},
					             {altD3_1, altD3_2, altD3_3, altD3_4},
					             {altD4_1, altD4_2, altD4_3, altD4_4},
					             {altD5_1, altD5_2, altD5_3, altD5_4},
					             {altD6_1, altD6_2, altD6_3, altD6_4},
					             {altD7_1, altD7_2, altD7_3, altD7_4},
					             {altD8_1, altD8_2, altD8_3, altD8_4},
					             {altD9_1, altD9_2, altD9_3, altD9_4}, 
					             {altD10_1, altD10_2, altD10_3, altD10_4},
					             {altD11_1, altD11_2, altD11_3, altD11_4},
					             {altD12_1, altD12_2, altD12_3, altD12_4},
					             {altD13_1, altD13_2, altD13_3, altD13_4},
					             {altD14_1, altD14_2, altD14_3, altD14_4},
					             {altD15_1, altD15_2, altD15_3, altD15_4},
					             {altD16_1, altD16_2, altD16_3, altD16_4},
					             {altD17_1, altD17_2, altD17_3, altD17_4},
					             {altD18_1, altD18_2, altD18_3, altD18_4},
					             {altD19_1, altD19_2, altD19_3, altD19_4},
					             {altD20_1, altD20_2, altD20_3, altD20_4},
					             {altD21_1, altD21_2, altD21_3, altD21_4},
					             {altD22_1, altD22_2, altD22_3, altD22_4},
					             {altD23_1, altD23_2, altD23_3, altD23_4},
					             {altD24_1, altD24_2, altD24_3, altD24_4},
					             {altD25_1, altD25_2, altD25_3, altD25_4}
			          		   };
	
	int *lista_respD[25] = {
				              RespCertaPergD1, RespCertaPergD2, RespCertaPergD3, RespCertaPergD4, RespCertaPergD5,
				              RespCertaPergD6, RespCertaPergD7, RespCertaPergD8, RespCertaPergD9, RespCertaPergD10,
				              RespCertaPergD11, RespCertaPergD12, RespCertaPergD13, RespCertaPergD14, RespCertaPergD15,
				              RespCertaPergD16, RespCertaPergD17, RespCertaPergD18, RespCertaPergD19, RespCertaPergD20,
				              RespCertaPergD21, RespCertaPergD22, RespCertaPergD23, RespCertaPergD24, RespCertaPergD25
				            };
	
	int lista_sorteadasD[25] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
//############################################################################################################################
//############################################################################################################################

	// +++++++++++++++++++++++++++
	//    DECLARA��O DE FUNCOES  + 
	// +++++++++++++++++++++++++++

		//=====================================================//
		// Fun��o Centraliza String e Converte em Mai�sculas:  //
		//=====================================================//
	
	char strCentroMaiuscula(char nome[100], int tamHorizontalTela){
		
		int i = 0;
		char espacos[100];
		
		for ( i = 0 ; i < ((tamHorizontalTela - strlen(nome)) / 2) ; i++ ){
			espacos[i] = ' ';
		}
		
		strcat(espacos, nome);
		
		printf("%s\n", strupr(espacos));
	}


									// +++++++++++++++++++++++++++
									// TELA INICIAL DO PROGRAMA: + 
									// +++++++++++++++++++++++++++

	//system("color 0F");
	
	printf ("================================================================================");
	printf ("\n");
	printf ("   ||                           ----------------                            || \n");
	printf ("   ||                          *** INTELLECTUS ***                          || \n");
	printf ("   ||                           ----------------                            || \n");
	printf ("   ||                                                                       || \n");
	printf ("   ||                 ----------------------------------                    || \n");
	printf ("   ||                   ... PROVE QUE N�O �S BURRO ...                      || \n");
	printf ("   ||                                 ...                                   || \n");
	printf ("   ||                      DESAFIE SUA INTELIG�NCIA!                        || \n");
	printf ("   ||                       -----------------------                         || \n");
	printf ("\n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	printf("...Pressione ENTER para ACEITAR o DESAFIO! >>> ");
	scanf("x");
	getchar();
	system("CLS");

//############################################################################################################################
//############################################################################################################################

									// ++++++++++++++++++++++++++++++
									// RECEBENDO O NOME DO JOGADOR: + 
									// ++++++++++++++++++++++++++++++

	//system("color F0");
	
	printf ("================================================================================");
	printf ("\n");
	printf ("   ||                           ----------------                            || \n");
	printf ("   ||                          *** INTELLECTUS ***                          || \n");
	printf ("   ||                           ----------------                            || \n");
	printf ("   ||                                                                       || \n");
	printf ("   ||                  ----------------------------------                   || \n");
	printf ("   ||                                                                       || \n");
	printf ("   ||           ... VAMOS CONHECER AGORA O NOSSO PARTICIPANTE ...           || \n");
	printf ("   ||                                                                       || \n");
	printf ("   ||                        ----------------------                         || \n");
	printf ("\n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
			
	printf("...DIGITE SEU NOME: >>> ");
	scanf("%[ -~]", &nomeJogador);
	getchar();
	system("CLS");

//############################################################################################################################
//############################################################################################################################

									// +++++++++++++++++++++++++++++++
									// IMPRIMINDO O NOME DO JOGADOR: + 
									// +++++++++++++++++++++++++++++++
	
	//system("color 0F");
	
	printf ("================================================================================");
	printf ("   ||                           ----------------                           || \n");
	printf ("   ||                          *** INTELECTUS ***                          || \n");
	printf ("   ||                           ----------------                           || \n");
	printf ("   ||                                                                      || \n");
	printf ("   ||                 ----------------------------------                   || \n");
	printf ("   ||                               PARAB�NS                               || \n");
	strCentroMaiuscula(nomeJogador, 80);															//Fun��o Centraliza String e Converte em Mai�sculas
	printf ("   ||           ... SABEMOS AO MENOS QUE VOC� TEM CORAGEM! ...             || \n");
	printf ("   ||                       -----------------------                        || \n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	
	printf("...Pressione ENTER para SE ARRISCAR UM POUCO MAIS... >>> ");
	scanf("x");
	getchar();
	system("CLS");

//############################################################################################################################
//############################################################################################################################
	
		// ***************************************************************************************************
		// *                               FUN��O ESTRUTURAL PARA PERGUNTAS                                  *
		// ***************************************************************************************************

	int pergunta(char *nomeJogador, int pontos, int erro, int acerto, char *Perg, char *alt_1, char *alt_2, char *alt_3, char *alt_4){
		
		int RespUsuarioPerg = 0;
		
		printf ("================================================================================");
		printf ("                               ..:: JOGADOR ::..                               \n");
		printf ("%37s\n", nomeJogador);															//Fun��o Centraliza String e Converte em Mai�sculas
		printf ("          -------------------------------------------------------------        \n");
		printf ("                              ||  PONTUA��O  ||");
		printf ("\n");
		printf ("\t|| CR�DITOS = R$ %d    ||-|| ERRAR = R$ %d ||-|| ACERTAR = R$ %d ||\n", pontos, erro, acerto);        
		printf ("================================================================================");
		printf ("                                <<< PERGUNTA >>>                               \n");
		printf ("================================================================================");
		printf ("\n");
		printf ("\n");
		printf ("\n");
		//printf ("                                                                               ");
		printf ("%s\n", Perg);
		//printf ("                                                                               ");
		printf ("\n");
		printf ("\n");
		printf ("\n");
		printf ("================================================================================");
		printf ("                              <<< ALTERNATIVAS >>>                              ");
		printf ("================================================================================");
		printf ("\n");
		printf (" 1 - %s\n", alt_1);
		printf (" 2 - %s\n", alt_2);
		printf (" 3 - %s\n", alt_3);
		printf (" 4 - %s\n", alt_4);
		printf ("\n");
		printf ("================================================================================");
		printf ("================================================================================");
		printf ("\n");
		printf ("\n");
		
		printf ("Qual � a resposta CERTA? >>> ");
		scanf("%d", &RespUsuarioPerg);
		getchar();
		system("CLS");
		
		return RespUsuarioPerg;
	}
        
//############################################################################################################################
//############################################################################################################################        

										// =================================== 
										//         TABELA DE PONTUA��O:      =
										//									 =
										//      ACERTO: RODADA A =   1.000   = 
										//      ACERTO: RODADA B =  10.000   = 
										//      ACERTO: RODADA C = 100.000   = 
										//      ACERTO: RODADA D = 1 MILH�O  = 
										//        ERRO: PONTOS / 4           =
										// ===================================

//############################################################################################################################
//############################################################################################################################

								// ======================================================
								// =                      SETOR							=
								// =					>>> A <<<  						=
								// =													=
								// ======================================================

						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// TELA DE ABERTURA: RODADA DE PERGUNTAS N�VEL >>> A <<<: + 
						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	printf ("================================================================================");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                           *** INTELECTUS ***                          || \n");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                                                                       || \n");
	printf ("  ||                  ----------------------------------                   || \n");
	printf ("  ||       VAMOS DAR IN�CIO A RODADA DE PERGUNTAS VALENDO R$ 1.000,00.     || \n");
	printf ("  ||                                  ...                                  || \n");
	printf ("  ||                           ... BOA SORTE! ...                          || \n");
	printf ("  ||                        -----------------------                        || \n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	
	printf("... Pressione ENTER para CONTINUAR... >>> ");
	scanf("x");
	getchar();
	system("CLS");
	
//############################################################################################################################
//############################################################################################################################


	NumerosPergA = 0;

	while (NumerosPergA < 5){ // DIZ-SE QUANTO AO N�MERO DE PERGUNTAS PARA CADA RODADA, OU SEJA, 5 PERGUNTAS.
	    
		srand(time(NULL));
		int RandomPergA = rand() % 25;
		
	    if (lista_sorteadasA[RandomPergA] == 0){
	    		erro = pontos / 4;
	            acerto = pontos + 1000;
	
	            int RespUsuarioPergA1 = pergunta(
												nomeJogador, 
												pontos, 
												erro, 
												acerto, 
												lista_perguntasA[RandomPergA], 
												lista_altA[RandomPergA][0], 
												lista_altA[RandomPergA][1], 
												lista_altA[RandomPergA][2], 
												lista_altA[RandomPergA][3]);

				// pergunta(nomeJogador, pontos, erro, acerto, lista_perguntasA[0], lista_altA[0][0], lista_altA[0][1], lista_altA[0][2], lista_altA[0][3]);
	
			if (RespUsuarioPergA1 == lista_respA[RandomPergA]){
                pontos = pontos + 1000;

                lista_sorteadasA[RandomPergA] = 1; //ALTERA-SE O VALOR DE 0 PARA 1: IMPEDE QUE A PERGUNTA CAIA EM RANDOM NOVAMENTE.
        
                NumerosPergA = NumerosPergA + 1;

                printf ("\n");
                printf ("                    PARAB�NS!!! VOC� ACERTOU...\n");
                printf ("\n");
                printf ("            Est� Preparado para a Pr�xima Pergunta?...\n");
                printf ("\n");
                printf ("\n");
                printf ("... Pressione ENTER para Continuar >>> ");
                
				scanf("x");
				getchar();
				system("CLS");
				
			} else {
			    
				printf ("\n");
                printf ("                    ERROU...\n");
                printf ("\n");
                printf ("\n");
                printf ("... Pressione ENTER para Continuar >>> ");

				scanf("x");
				getchar();
				system("CLS");
				exit(1);
			}	            
		}
	}


//############################################################################################################################
//############################################################################################################################
	
								// ======================================================
								// =                      SETOR							=
								// =					>>> B <<<  						=
								// =													=
								// ======================================================

						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// TELA DE ABERTURA: RODADA DE PERGUNTAS N�VEL >>> B <<<: + 
						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	printf ("================================================================================");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                           *** INTELECTUS ***                          || \n");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                                                                       || \n");
	printf ("  ||                  ----------------------------------                   || \n");
	printf ("  ||      VAMOS DAR IN�CIO A RODADA DE PERGUNTAS VALENDO R$ 10.000,00.     || \n");
	printf ("  ||                                  ...                                  || \n");
	printf ("  ||                           ... BOA SORTE! ...                          || \n");
	printf ("  ||                        -----------------------                        || \n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	
	printf("... Pressione ENTER para CONTINUAR... >>> ");
	scanf("x");
	getchar();
	system("CLS");

//############################################################################################################################
//############################################################################################################################

	NumerosPergB = 0;

	while (NumerosPergB < 5){ // DIZ-SE QUANTO AO N�MERO DE PERGUNTAS PARA CADA RODADA, OU SEJA, 5 PERGUNTAS.
	    
		srand(time(NULL));
		int RandomPergB = rand() % 25;
		
	    if (lista_sorteadasB[RandomPergB] == 0){
	    		erro = pontos / 4;
	            acerto = pontos + 10000;
	
	            int RespUsuarioPergB1 = pergunta(
												nomeJogador, 
												pontos, 
												erro, 
												acerto, 
												lista_perguntasB[RandomPergB], 
												lista_altB[RandomPergB][0], 
												lista_altB[RandomPergB][1], 
												lista_altB[RandomPergB][2], 
												lista_altB[RandomPergB][3]);
	
			if (RespUsuarioPergB1 == lista_respB[RandomPergB]){
                pontos = pontos + 10000;

                lista_sorteadasB[RandomPergB] = 1; //ALTERA-SE O VALOR DE 0 PARA 1: IMPEDE QUE A PERGUNTA CAIA EM RANDOM NOVAMENTE.
        
                NumerosPergB = NumerosPergB + 1;

                printf ("\n");
                printf ("                    PARAB�NS!!! VOC� ACERTOU...\n");
                printf ("\n");
                printf ("            Est� Preparado para a Pr�xima Pergunta?...\n");
                printf ("\n");
                printf ("\n");
                printf ("... Pressione ENTER para Continuar >>> ");
                
				scanf("x");
				getchar();
				system("CLS");
				
			} else {
			    
				printf ("\n");
                printf ("                    ERROU...\n");
                printf ("\n");
                printf ("\n");
                printf ("... Pressione ENTER para Continuar >>> ");

				scanf("x");
				getchar();
				system("CLS");
				exit(1);
			}	            
		}
	}

//############################################################################################################################
//############################################################################################################################
								// ======================================================
								// =                      SETOR							=
								// =					>>> C <<<  						=
								// =													=
								// ======================================================

						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// TELA DE ABERTURA: RODADA DE PERGUNTAS N�VEL >>> C <<<: + 
						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	printf ("================================================================================");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                           *** INTELECTUS ***                          || \n");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                                                                       || \n");
	printf ("  ||                  ----------------------------------                   || \n");
	printf ("  ||     VAMOS DAR IN�CIO A RODADA DE PERGUNTAS VALENDO R$ 100.000,00.     || \n");
	printf ("  ||                                  ...                                  || \n");
	printf ("  ||                           ... BOA SORTE! ...                          || \n");
	printf ("  ||                        -----------------------                        || \n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	
	printf("... Pressione ENTER para CONTINUAR... >>> ");
	scanf("x");
	getchar();
	system("CLS");

//############################################################################################################################
//############################################################################################################################

	NumerosPergC = 0;

	while (NumerosPergC < 5){ // DIZ-SE QUANTO AO N�MERO DE PERGUNTAS PARA CADA RODADA, OU SEJA, 5 PERGUNTAS.
	    
		srand(time(NULL));
		int RandomPergC = rand() % 25;
		
	    if (lista_sorteadasC[RandomPergC] == 0){
	    		erro = pontos / 4;
	            acerto = pontos + 100000;
	
	            int RespUsuarioPergC1 = pergunta(
												nomeJogador, 
												pontos, 
												erro, 
												acerto, 
												lista_perguntasC[RandomPergC], 
												lista_altC[RandomPergC][0], 
												lista_altC[RandomPergC][1], 
												lista_altC[RandomPergC][2], 
												lista_altC[RandomPergC][3]);
	
			if (RespUsuarioPergC1 == lista_respC[RandomPergC]){
                pontos = pontos + 100000;

                lista_sorteadasC[RandomPergC] = 1; //ALTERA-SE O VALOR DE 0 PARA 1: IMPEDE QUE A PERGUNTA CAIA EM RANDOM NOVAMENTE.
        
                NumerosPergC = NumerosPergC + 1;

                printf ("\n");
                printf ("                    PARAB�NS!!! VOC� ACERTOU...\n");
                printf ("\n");
                printf ("            Est� Preparado para a Pr�xima Pergunta?...\n");
                printf ("\n");
                printf ("\n");
                printf ("... Pressione ENTER para Continuar >>> ");
                
				scanf("x");
				getchar();
				system("CLS");
				
			} else {
			    
				printf ("\n");
                printf ("                    ERROU...\n");
                printf ("\n");
                printf ("\n");
                printf ("... Pressione ENTER para Continuar >>> ");

				scanf("x");
				getchar();
				system("CLS");
				exit(1);
			}	            
		}
	}

//############################################################################################################################
//############################################################################################################################	
								// ======================================================
								// =                      SETOR							=
								// =					>>> D <<<  						=
								// =													=
								// ======================================================

						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// TELA DE ABERTURA: RODADA DE PERGUNTAS N�VEL >>> D <<<: + 
						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	printf ("================================================================================");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                           *** INTELECTUS ***                          || \n");
	printf ("  ||                            ----------------                           || \n");
	printf ("  ||                                                                       || \n");
	printf ("  ||                  ----------------------------------                   || \n");
	printf ("  ||        VAMOS DAR IN�CIO A RODADA DE PERGUNTAS VALENDO 1 MILH�O!!!     || \n");
	printf ("  ||                                  ...                                  || \n");
	printf ("  ||                           ... BOA SORTE! ...                          || \n");
	printf ("  ||                        -----------------------                        || \n");
	printf ("================================================================================");
	printf ("\n");
	printf ("\n");
	printf ("\n");
	
	printf("... Pressione ENTER para CONTINUAR... >>> ");
	scanf("x");
	getchar();
	system("CLS");

//############################################################################################################################
//############################################################################################################################
	
						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						// 					TELA DE PARABENIZA��O				  + 
						// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
						
	void parabens(char *nomeJogador, int rodadaFim)
	{
		printf ("================================================================================");
		printf ("\n");
		printf ("  ||                            ----------------                            || \n");
		printf ("  ||                         *** INTELECTUS 0.3 ***                         || \n");
		printf ("  ||                            ----------------                            || \n");
		printf ("  ||                                                                        || \n");
		printf ("  ||                  ----------------------------------                    || \n");
		printf ("  ||                                PARAB�NS                                || \n");
		printf ("%s\n", nomeJogador);
		printf ("  ||          ... VOC� ACERTOU TODAS AS PERGUNTAS DA %d� RODADA ...         || \n", rodadaFim);
		printf ("  ||                        -----------------------                         || \n");
		printf ("================================================================================");
		printf ("\n");
		printf ("\n");
		printf ("\n");
		printf ("\n");
	}
	
	parabens (nomeJogador, 99);

//############################################################################################################################
//############################################################################################################################

	NumerosPergD = 0;

	while (NumerosPergD < 1){
	    
		srand(time(NULL));
		int RandomPergD = rand() % 25;
		
	    if (lista_sorteadasD[RandomPergD] == 0){
	    		erro = pontos / 4;
	            acerto = pontos + 500000;
	
	            int RespUsuarioPergD1 = pergunta(
												nomeJogador, 
												pontos, 
												erro, 
												acerto, 
												lista_perguntasD[RandomPergD], 
												lista_altD[RandomPergD][0], 
												lista_altD[RandomPergD][1], 
												lista_altD[RandomPergD][2], 
												lista_altD[RandomPergD][3]);
	
			if (RespUsuarioPergD1 == lista_respD[RandomPergD]){
                pontos = pontos + 100000;

                lista_sorteadasD[RandomPergD] = 1; //ALTERA-SE O VALOR DE 0 PARA 1: IMPEDE QUE A PERGUNTA CAIA EM RANDOM NOVAMENTE.
        
                NumerosPergD = NumerosPergD + 1;

                printf ("\n");
                printf ("                    PARAB�NS!!! VOC� ACERTOU...\n");
                printf ("\n");
                printf ("            Est� Preparado para a Pr�xima Pergunta?...\n");
                printf ("\n");
                printf ("\n");
                printf ("... Pressione ENTER para Continuar >>> ");
                
				scanf("x");
				getchar();
				system("CLS");
				
			} else {
			    
				printf ("\n");
                printf ("                    ERROU...\n");
                printf ("\n");
                printf ("\n");
                printf ("... Pressione ENTER para Continuar >>> ");

				scanf("x");
				getchar();
				system("CLS");
				exit(1);
			}	            
		}
	}

	return 0;	
}
