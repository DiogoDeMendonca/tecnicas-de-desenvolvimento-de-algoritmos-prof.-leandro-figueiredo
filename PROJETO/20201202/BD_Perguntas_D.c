#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>
#include "BD_Perguntas_D.h"

// Gera as Perguntas e Respostas Aleat�rias:
void geraPerguntaD(char ** pergunta, char ** alternativa1, char ** alternativa2, char ** alternativa3, char ** alternativa4){

	char PergD1[200] = "QUEM TEM MEDO DE C�ES SOFRE DE:";
	char altD1_1[100] = "CINOFOBIA";
	char altD1_2[100] = "CANILFOBIA";
	char altD1_3[100] = "C�OFOBIA";
	char altD1_4[100] = "CATFOBIA";
	int RespCertaPergD1 = 1;	
	int pergSorteada_D1 = 0;
				
	char PergD2[200] = "O QUE SIGNIFICA DIUTURNO?";
	char altD2_1[100] = "QUE VIVE DE DIA E DE NOITE";
	char altD2_2[100] = "QUE MORRE CEDO";
	char altD2_3[100] = "QUE VIVE MUITO TEMPO";
	char altD2_4[100] = "QUE VIVE DE DIA";
	int RespCertaPergD2 = 3;
	int pergSorteada_D2 = 0;
				
	char PergD3[200] = "QUE NOME RECEBE A ARTE DE GRAVAR EM PEDRAS PRECIOSAS?";
	char altD3_1[100] = "GL�PTICA";
	char altD3_2[100] = "FORMARTE";
	char altD3_3[100] = "CL�PTICA";
	char altD3_4[100] = "GRAFITA";
	int RespCertaPergD3 = 1;	
	int pergSorteada_D3 = 0;
				
	char PergD4[200] = "QUE NOME RECEBE A CONTRADI��O ENTRE DUAS LEIS?";
	char altD4_1[100] = "ANTINOMIA";
	char altD4_2[100] = "AUT�NOMO";
	char altD4_3[100] = "ANT�NIMO";
	char altD4_4[100] = "PAR�DIA";
	int RespCertaPergD4 = 1;	
	int pergSorteada_D4 = 0;
				
	char PergD5[200] = "QUEM DISSE: 'S� SEI QUE NADA SEI'?";
	char altD5_1[100] = "FREUD";
	char altD5_2[100] = "S�CRATES";
	char altD5_3[100] = "PLAT�O";
	char altD5_4[100] = "ARIST�TELES";
	int RespCertaPergD5 = 2;
	int pergSorteada_D5 = 0;
				
	char PergD6[200] = "O D�NAMO TRANSFORMA A ENERGIA MEC�NICA EM:";
	char altD6_1[100] = "NATURAL";
	char altD6_2[100] = "HUMANA";
	char altD6_3[100] = "EL�TRICA";
	char altD6_4[100] = "CIN�TICA";
	int RespCertaPergD6 = 3;	
	int pergSorteada_D6 = 0;
				
	char PergD7[200] = "SEGUNDO A B�BLIA, QUAL APOST�LO ERA ENCARREGADO DE CUIDAR DO DINHEIRO DO GRUPO?";
	char altD7_1[100] = "JO�O";
	char altD7_2[100] = "MATEUS";
	char altD7_3[100] = "JUDAS";
	char altD7_4[100] = "PEDRO";
	int RespCertaPergD7 = 3;	
	int pergSorteada_D7 = 0;
				
	char PergD8[200] = "TAUMATURGO � AQUELE QUE:";
	char altD8_1[100] = "N�O TRABALHA";
	char altD8_2[100] = "TRABALHA DE DIA";
	char altD8_3[100] = "FAZ MILAGRES";
	char altD8_4[100] = "TRABALHA MUITO";
	int RespCertaPergD8 = 3;
	int pergSorteada_D8 = 0;
				
	char PergD9[200] = "QUEM COMP�S A �PERA 'CARMEN'?";
	char altD9_1[100] = "MOZART";
	char altD9_2[100] = "GIUSEPPE VERDI";
	char altD9_3[100] = "GEORGES BIZET";
	char altD9_4[100] = "MAURICE RAVEL";
	int RespCertaPergD9 = 3;
	int pergSorteada_D9 = 0;
		 			
	char PergD10[200] = "POR ONDE AS BORBOLETAS SE ALIMENTAM?";
	char altD10_1[100] = "PROB�SCIDE";
	char altD10_2[100] = "ASAS";
	char altD10_3[100] = "OLHOS";
	char altD10_4[100] = "ANTENAS";
	int RespCertaPergD10 = 1;		
	int pergSorteada_D10 = 0;
				
	char PergD11[200] = "OS CURSOS FLUVIAIS TIGRE E EUFRATES EST�O LOCALIZADOS NA:";
	char altD11_1[100] = "AM�RICA DO NORTE";
	char altD11_2[100] = "AM�RICA CENTRAL";
	char altD11_3[100] = "AM�RICA DO SUL";
	char altD11_4[100] = "�SIA OCIDENTAL";
	int RespCertaPergD11 = 4;
	int pergSorteada_D11 = 0;
	
	char PergD12[200] = "QUAIS S�O AS CORES DA BANDEIRA DO PAQUIST�O?";
	char altD12_1[100] = "AZUL E LARANJA";
	char altD12_2[100] = "BRANCO E VERDE";
	char altD12_3[100] = "VERMELHO E VERDE";
	char altD12_4[100] = "AMARELO E AZUL";
	int RespCertaPergD12 = 2;
	int pergSorteada_D12 = 0;
	
	char PergD13[200] = "QUEM TEM OLHOS NEGROS � CHAMADO DE QU�?";
	char altD13_1[100] = "MELANITA";
	char altD13_2[100] = "MELANOPE";
	char altD13_3[100] = "MELAN�TICO";
	char altD13_4[100] = "MELAN�RIA";
	int RespCertaPergD13 = 2;	
	int pergSorteada_D13 = 0;
				
	char PergD14[200] = "QUAL DESSES FIL�SOFOS N�O � CONSIDERADO ILUMINISTA?";
	char altD14_1[100] = "ROUSSEAU";
	char altD14_2[100] = "BAKUNIN";
	char altD14_3[100] = "MONTESQUIEU";
	char altD14_4[100] = "VOLTAIRE";
	int RespCertaPergD14 = 2;
	int pergSorteada_D14 = 0;
	
	char PergD15[200] = "QUEM FOI TALES DE MILETO?";
	char altD15_1[100] = "PINTOR";
	char altD15_2[100] = "FIL�SOFO";
	char altD15_3[100] = "CANTOR";
	char altD15_4[100] = "IMPERADOR";
	int RespCertaPergD15 = 2;	
	int pergSorteada_D15 = 0;
				
	char PergD16[200] = "ONDE NASCEU O POETA BRASILEIRO GON�ALVES DIAS?";
	char altD16_1[100] = "MARANH�O";
	char altD16_2[100] = "S�O PAULO";
	char altD16_3[100] = "RIO DE JANEIRO";
	char altD16_4[100] = "MINAS GERAIS";
	int RespCertaPergD16 = 1;
	int pergSorteada_D16 = 0;
				
	char PergD17[200] = "O QUE SIGNIFICA EULALIA?";
	char altD17_1[100] = "BAGUN�A";
	char altD17_2[100] = "DOEN�A";
	char altD17_3[100] = "BOA DIC��O";
	char altD17_4[100] = "EUFORIA";
	int RespCertaPergD17 = 3;		
	int pergSorteada_D17 = 0;
				
	char PergD18[200] = "TIA EM QUARTO GRAU �:";
	char altD18_1[100] = "TETRAZINA";
	char altD18_2[100] = "ARAMITA";
	char altD18_3[100] = "ADIT�CIA";
	char altD18_4[100] = "AB�MITA";
	int RespCertaPergD18 = 4;		
	int pergSorteada_D18 = 0;
	
	char PergD19[200] = "QUAL DESTES N�O � UM ELEMENTO QU�MICO?";
	char altD19_1[100] = "TEL�RIO";
	char altD19_2[100] = "PERI�LIO";
	char altD19_3[100] = "VAN�DIO";
	char altD19_4[100] = "ESTR�NCIO";
	int RespCertaPergD19 = 2;	
	int pergSorteada_D19 = 0;
				
	char PergD20[200] = "QUEM DESCOBRIU OS RAIOS X?";
	char altD20_1[100] = "GALILEU";
	char altD20_2[100] = "THOMAS EDISON";
	char altD20_3[100] = "GRAHAM BELL";
	char altD20_4[100] = "WILHELM R�NTGEN";
	int RespCertaPergD20 = 4;
	int pergSorteada_D20 = 0;
							
	char PergD21[200] = "A GR�CIA FICA EM QUAL REGI�O DA EUROPA?";
	char altD21_1[100] = "SETENTRIONAL";
	char altD21_2[100] = "OCIDENTAL";
	char altD21_3[100] = "ORIENTAL";
	char altD21_4[100] = "MERIDIONAL";
	int RespCertaPergD21 = 4;	
	int pergSorteada_D21 = 0;
				
	char PergD22[200] = "QUAL O NOME DO MOVIMENTO FILOS�FICO SURGIDO NO S�CULO XVIII QUE ERA BASEADO NO USO DA RAZ�O?";
	char altD22_1[100] = "ILUMINISMO";
	char altD22_2[100] = "PROTESTANTISMO";
	char altD22_3[100] = "CALVINISMO";
	char altD22_4[100] = "CATOLICISMO";
	int RespCertaPergD22 = 1;	
	int pergSorteada_D22 = 0;
					 		
	char PergD23[200] = "EM QUAL D�CADA OCORREU A REVOLU��O CUBANA?";
	char altD23_1[100] = "1940";
	char altD23_2[100] = "1970";
	char altD23_3[100] = "1950";
	char altD23_4[100] = "1960";
	int RespCertaPergD23 = 3;		
	int pergSorteada_D23 = 0;
				
	char PergD24[200] = "EM QUE ANO MORREU ADOLF HITLER?";
	char altD24_1[100] = "1945";
	char altD24_2[100] = "1939";
	char altD24_3[100] = "1933";
	char altD24_4[100] = "1960";
	int RespCertaPergD24 = 1;
	int pergSorteada_D24 = 0;
				
	char PergD25[200] = "A DEUSA DA SABEDORIA ERA:";
	char altD25_1[100] = "ATENA";
	char altD25_2[100] = "HEBE";
	char altD25_3[100] = "AFRODITE";
	char altD25_4[100] = "ANT�GONA";
	int RespCertaPergD25 = 1;
	int pergSorteada_D25 = 0;
	
	char *lista_perguntasD[25] = {
									PergD1, PergD2, PergD3, PergD4, PergD5,
				                    PergD6, PergD7, PergD8, PergD9, PergD10,
				                    PergD11, PergD12, PergD13, PergD14, PergD15,
				                    PergD16, PergD17, PergD18, PergD19, PergD20,
				                    PergD21, PergD22, PergD23, PergD24, PergD25
							 	 };
						
	char *lista_altD[25][4] = {
					             {altD1_1, altD1_2, altD1_3, altD1_4},
					             {altD2_1, altD2_2, altD2_3, altD2_4},
					             {altD3_1, altD3_2, altD3_3, altD3_4},
					             {altD4_1, altD4_2, altD4_3, altD4_4},
					             {altD5_1, altD5_2, altD5_3, altD5_4},
					             {altD6_1, altD6_2, altD6_3, altD6_4},
					             {altD7_1, altD7_2, altD7_3, altD7_4},
					             {altD8_1, altD8_2, altD8_3, altD8_4},
					             {altD9_1, altD9_2, altD9_3, altD9_4}, 
					             {altD10_1, altD10_2, altD10_3, altD10_4},
					             {altD11_1, altD11_2, altD11_3, altD11_4},
					             {altD12_1, altD12_2, altD12_3, altD12_4},
					             {altD13_1, altD13_2, altD13_3, altD13_4},
					             {altD14_1, altD14_2, altD14_3, altD14_4},
					             {altD15_1, altD15_2, altD15_3, altD15_4},
					             {altD16_1, altD16_2, altD16_3, altD16_4},
					             {altD17_1, altD17_2, altD17_3, altD17_4},
					             {altD18_1, altD18_2, altD18_3, altD18_4},
					             {altD19_1, altD19_2, altD19_3, altD19_4},
					             {altD20_1, altD20_2, altD20_3, altD20_4},
					             {altD21_1, altD21_2, altD21_3, altD21_4},
					             {altD22_1, altD22_2, altD22_3, altD22_4},
					             {altD23_1, altD23_2, altD23_3, altD23_4},
					             {altD24_1, altD24_2, altD24_3, altD24_4},
					             {altD25_1, altD25_2, altD25_3, altD25_4}
			          		   };
	
	int *lista_respD[25] = {
				              RespCertaPergD1, RespCertaPergD2, RespCertaPergD3, RespCertaPergD4, RespCertaPergD5,
				              RespCertaPergD6, RespCertaPergD7, RespCertaPergD8, RespCertaPergD9, RespCertaPergD10,
				              RespCertaPergD11, RespCertaPergD12, RespCertaPergD13, RespCertaPergD14, RespCertaPergD15,
				              RespCertaPergD16, RespCertaPergD17, RespCertaPergD18, RespCertaPergD19, RespCertaPergD20,
				              RespCertaPergD21, RespCertaPergD22, RespCertaPergD23, RespCertaPergD24, RespCertaPergD25
				            };
	
	int lista_sorteadasD[25] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	// Realizando A��o Aleat�ria: 
	
	int perguntasGeradas = 0;
	
	while (perguntasGeradas < 1){

		srand(time(NULL));
		
		int RandomPergD = rand() % 25;
		
		if (lista_sorteadasD[RandomPergD] == 0){
		
			*pergunta 		= lista_perguntasD[RandomPergD];
			*alternativa1	= lista_altD[RandomPergD][0];
			*alternativa2	= lista_altD[RandomPergD][1];
			*alternativa3	= lista_altD[RandomPergD][2];
			*alternativa4	= lista_altD[RandomPergD][3];
			
			lista_sorteadasD[RandomPergD] = 1;
			
			perguntasGeradas =+1;
		}

	}
	
}
// 

