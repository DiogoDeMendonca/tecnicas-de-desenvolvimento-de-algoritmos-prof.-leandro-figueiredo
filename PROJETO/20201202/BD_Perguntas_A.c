#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>
#include "BD_Perguntas_A.h"

// Gera as Perguntas e Respostas Aleat�rias:
int geraPerguntaA(int *saldoCarteira, int *saldoAcertar, int *saldoErrar, char ** pergunta, char ** alternativa1, char ** alternativa2, char ** alternativa3, char ** alternativa4, int * RANDOM){

	char PergA1[200] = "QUAL PERSONAGEM DA TURMA DO CHAVES VIVE COBRANDO O ALUGUEL AO SEU MADRUGA?";
	char altA1_1[100] = "KIKO";
	char altA1_2[100] = "CHAVES";
	char altA1_3[100] = "SEU BARRIGA";
	char altA1_4[100] = "PROFESSOR GIRAFALES";
	int RespCertaPergA1 = 3;
	int pergSorteada_A1 = 0;	// 0 - DIZ-SE QUANDO A PERGUNTA AINDA N�O FOI SORTEADA.
	                    		// 1 - DIZ-SE QUANDO A PERGUNTA J� FOI SORTEADA, PARA QUE N�O SEJA REPETIDA, ALTERA-SE SEU VALOR DE 0 PARA 1.
	
	char PergA2[200] = "QUAL PA�S FIDEL CASTRO REVOLUCIONOU JUNTO COM CHE GUEVARA?";
	char altA2_1[100] = "JAMAICA";
	char altA2_2[100] = "CUBA";
	char altA2_3[100] = "EL SALVADOR";
	char altA2_4[100] = "M�XICO";
	int RespCertaPergA2 = 2;
	int pergSorteada_A2 = 0;
	
	char PergA3[200] = "QUAL A COR ASSOCIADA A GRUPOS ECOL�GICOS?";
	char altA3_1[100] = "PRETO";
	char altA3_2[100] = "VERMELHO";
	char altA3_3[100] = "AZUL";
	char altA3_4[100] = "VERDE";
	int RespCertaPergA3 = 4;
	int pergSorteada_A3 = 0;
	
	char PergA4[200] = "COM QUANTOS GRAUS CENT�GRADOS A �GUA FERVE?";
	char altA4_1[100] = "200";
	char altA4_2[100] = "100";
	char altA4_3[100] = "170";
	char altA4_4[100] = "220";
	int RespCertaPergA4 = 2;
	int pergSorteada_A4 = 0;
	
	char PergA5[200] = "QUANDO � COMEMORADO O DIA DA INDEPEND�NCIA DO BRASIL?";
	char altA5_1[100] = "21 DE ABRIL";
	char altA5_2[100] = "12 DE OUTUBRO";
	char altA5_3[100] = "7 DE SETEMBRO";
	char altA5_4[100] = "25 DE DEZEMBRO";
	int RespCertaPergA5 = 3;
	int pergSorteada_A5 = 0;
	
	char PergA6[200] = "QUAL O NOME DA �REA EM ROMA NA QUAL VIVE O PAPA?";
	char altA6_1[100] = "VENEZA";
	char altA6_2[100] = "VIT�RIA";
	char altA6_3[100] = "VANCOUVER";
	char altA6_4[100] = "VATICANO";
	int RespCertaPergA6 = 4;
	int pergSorteada_A6 = 0;
					
	char PergA7[200] = "QUE FERIADO � COMEMORADO NO DIA 1� DE MAIO?";
	char altA7_1[100] = "DIA DO AVIADOR";
	char altA7_2[100] = "DIA DO TRABALHO";
	char altA7_3[100] = "DIA DAS M�ES";
	char altA7_4[100] = "DIA DA BANDEIRA";
	int RespCertaPergA7 = 2;
	int pergSorteada_A7 = 0;
				
	char PergA8[200] = "QUEM INVENTOU O TELEFONE?";
	char altA8_1[100] = "GRAHAM BELL";
	char altA8_2[100] = "G. WASHINGTON";
	char altA8_3[100] = "TOMAS EDISON";
	char altA8_4[100] = "MARCONI";
	int RespCertaPergA8 = 1;
	int pergSorteada_A8 = 0;
	
	char PergA9[200] = "QUAL  PERSONAGEM DA TURMA DA M�NICA  TEM  CINCO FIOS DE CABELO?";
	char altA9_1[100] = "M�NICA";
	char altA9_2[100] = "CEBOLINHA";
	char altA9_3[100] = "CASC�O";
	char altA9_4[100] = "MAGALI";
	int RespCertaPergA9 = 2;
	int pergSorteada_A9 = 0;
		 			
	char PergA10[200] = "QUEM CRIOU OS PERSONAGENS PEDRINHO E NARIZINHO?";
	char altA10_1[100] = "M. DE SOUSA";
	char altA10_2[100] = "ZIRALDO";
	char altA10_3[100] = "M. LOBATO";
	char altA10_4[100] = "M. C. MACHADO";
	int RespCertaPergA10 = 3;
	int pergSorteada_A10 = 0;
				
	char PergA11[200] = "QUAL � O HOMEM MAIS RICO DO MUNDO ATUALMENTE?";
	char altA11_1[100] = "SULT�O DE BRUNEI";
	char altA11_2[100] = "AKIO MORITA";
	char altA11_3[100] = "BILL GATES";
	char altA11_4[100] = "O DONO DO MANA�RA SHOPPING";
	int RespCertaPergA11 = 3;
	int pergSorteada_A11 = 0;
				
	char PergA12[200] = "QUEM CRIOU OS PERSONAGENS DA TURMA DA M�NICA?";
	char altA12_1[100] = "MONTEIRO LOBATO";
	char altA12_2[100] = "JORGE AMADO";
	char altA12_3[100] = "MAUR�CIO DE SOUSA";
	char altA12_4[100] = "ANGELI";
	int RespCertaPergA12 = 3;
	int pergSorteada_A12 = 0;
	
	char PergA13[200] = "QUAL � A PEDRA PRECIOSA MAIS DURA ENCONTRADA NA NATUREZA?";
	char altA13_1[100] = "ESMERALDA";
	char altA13_2[100] = "RUBI";
	char altA13_3[100] = "P�ROLA";
	char altA13_4[100] = "DIAMANTE";
	int RespCertaPergA13 = 4;
	int pergSorteada_A13 = 0;
				
	char PergA14[200] = "QUE CANTOR AMERICANO FICOU CONHECIDO COMO 'O REI DO ROCK'?";
	char altA14_1[100] = "FRANK SINATRA";
	char altA14_2[100] = "LITTLE RICHARD";
	char altA14_3[100] = "ELVIS PRESLEY";
	char altA14_4[100] = "RICHIE VALENS";
	int RespCertaPergA14 = 3;
	int pergSorteada_A14 = 0;
	
	char PergA15[200] = "QUANTOS DIAS TEM UM ANO BISSEXTO?";
	char altA15_1[100] = "364";
	char altA15_2[100] = "365";
	char altA15_3[100] = "366";
	char altA15_4[100] = "367";
	int RespCertaPergA15 = 3;
	int pergSorteada_A15 = 0;
				
	char PergA16[200] = "QUAL � A CAPITAL DO CINEMA MUNDIAL?";
	char altA16_1[100] = "GRAMADO";
	char altA16_2[100] = "LAS VEGAS";
	char altA16_3[100] = "NEW ORLEANS";
	char altA16_4[100] = "HOLLYWOOD";
	int RespCertaPergA16 = 4;
	int pergSorteada_A16 = 0;
			
	char PergA17[200] = "QUE DIA � COMEMORADO A PROCLAMA��O DA REP�BLICA NO BRASIL?";
	char altA17_1[100] = "19 DE ABRIL";
	char altA17_2[100] = "15 DE OUTUBRO";
	char altA17_3[100] = "19 DE NOVEMBRO";
	char altA17_4[100] = "15 DE NOVEMBRO";
	int RespCertaPergA17 = 4;
	int pergSorteada_A17 = 0;
					
	char PergA18[200] = "DE QUANTO O BRASIL PERDEU DA FRAN�A NA FINAL DA COPA DE 98?";
	char altA18_1[100] = "2 X 0";
	char altA18_2[100] = "3 X 0";
	char altA18_3[100] = "4 X 0";
	char altA18_4[100] = "5 X 0";
	int RespCertaPergA18 = 2;
	int pergSorteada_A18 = 0;
	
	char PergA19[200] = "O QUE VEM DEPOIS DO VER�O E ANTES DO INVERNO?";
	char altA19_1[100] = "OUTONO";
	char altA19_2[100] = "PRIMAVERA";
	char altA19_3[100] = "INVERNO";
	char altA19_4[100] = "VER�O";
	int RespCertaPergA19 = 1;
	int pergSorteada_A19 = 0;
	
	char PergA20[200] = "VIOLONCELO � UM INSTRUMENTO DE:";
	char altA20_1[100] = "SOPRO";
	char altA20_2[100] = "CORDAS";
	char altA20_3[100] = "PERCUSS�O";
	char altA20_4[100] = "REPERCUSS�O";
	int RespCertaPergA20 = 2;
	int pergSorteada_A20 = 0;
				
	char PergA21[200] = "QUAL �REA DA MEDICINA QUE TRATA AS CRIAN�AS?";
	char altA21_1[100] = "GERIATRIA";
	char altA21_2[100] = "PEDIATRIA";
	char altA21_3[100] = "INFANTOLOGIA";
	char altA21_4[100] = "BIOLOGIA";
	int RespCertaPergA21 = 2;
	int pergSorteada_A21 = 0;
	
	char PergA22[200] = "O SAQU� � UMA BEBIDA ORIGIN�RIA DE QUE PA�S?";
	char altA22_1[100] = "ESPANHA";
	char altA22_2[100] = "JAP�O";
	char altA22_3[100] = "COR�IA DO SUL";
	char altA22_4[100] = "CHINA";
	int RespCertaPergA22 = 2;
	int pergSorteada_A22 = 0;
		 		
	char PergA23[200] = "UM ADULTO SADIO TEM QUANTOS DENTES NA BOCA?";
	char altA23_1[100] = "18 DENTES";
	char altA23_2[100] = "24 DENTES";
	char altA23_3[100] = "32 DENTES";
	char altA23_4[100] = "36 DENTES";
	int RespCertaPergA23 = 3;
	int pergSorteada_A23 = 0;
	
	char PergA24[200] = "TURMALINA � UMA ESP�CIE DE QU�?";
	char altA24_1[100] = "FLOR";
	char altA24_2[100] = "FRUTA";
	char altA24_3[100] = "PEDRA";
	char altA24_4[100] = "VERDURA";
	int RespCertaPergA24 = 3;
	int pergSorteada_A24 = 0;
	
	char PergA25[200] = "QUE TIPO DE INSTRUMENTO � O PIANO?";
	char altA25_1[100] = "SOPRO";
	char altA25_2[100] = "CORDA";
	char altA25_3[100] = "PERCUSS�O";
	char altA25_4[100] = "FOR�A";
	int RespCertaPergA25 = 2;
	int pergSorteada_A25 = 0;
	
	char *lista_perguntasA[25] = { 
									PergA1, PergA2, PergA3, PergA4, PergA5, 
									PergA6, PergA7, PergA8, PergA9, PergA10, 
									PergA11, PergA12, PergA13, PergA14, PergA15,
									PergA16, PergA17, PergA18, PergA19, PergA20, 
									PergA21, PergA22, PergA23, PergA24, PergA25
								 };
	
	char *lista_altA[25][4] = {
					             {altA1_1, altA1_2, altA1_3, altA1_4},
					             {altA2_1, altA2_2, altA2_3, altA2_4},
					             {altA3_1, altA3_2, altA3_3, altA3_4},
					             {altA4_1, altA4_2, altA4_3, altA4_4},
					             {altA5_1, altA5_2, altA5_3, altA5_4},
					             {altA6_1, altA6_2, altA6_3, altA6_4},
					             {altA7_1, altA7_2, altA7_3, altA7_4},
					             {altA8_1, altA8_2, altA8_3, altA8_4},
					             {altA9_1, altA9_2, altA9_3, altA9_4}, 
					             {altA10_1, altA10_2, altA10_3, altA10_4},
					             {altA11_1, altA11_2, altA11_3, altA11_4},
					             {altA12_1, altA12_2, altA12_3, altA12_4},
					             {altA13_1, altA13_2, altA13_3, altA13_4},
					             {altA14_1, altA14_2, altA14_3, altA14_4},
					             {altA15_1, altA15_2, altA15_3, altA15_4},
					             {altA16_1, altA16_2, altA16_3, altA16_4},
					             {altA17_1, altA17_2, altA17_3, altA17_4},
					             {altA18_1, altA18_2, altA18_3, altA18_4},
					             {altA19_1, altA19_2, altA19_3, altA19_4},
					             {altA20_1, altA20_2, altA20_3, altA20_4},
					             {altA21_1, altA21_2, altA21_3, altA21_4},
					             {altA22_1, altA22_2, altA22_3, altA22_4},
					             {altA23_1, altA23_2, altA23_3, altA23_4},
					             {altA24_1, altA24_2, altA24_3, altA24_4},
					             {altA25_1, altA25_2, altA25_3, altA25_4}
	};
	
	int *lista_respA[25] = 	{
				              RespCertaPergA1, RespCertaPergA2, RespCertaPergA3, RespCertaPergA4, RespCertaPergA5,
				              RespCertaPergA6, RespCertaPergA7, RespCertaPergA8, RespCertaPergA9, RespCertaPergA10,
				              RespCertaPergA11, RespCertaPergA12, RespCertaPergA13, RespCertaPergA14, RespCertaPergA15,
				              RespCertaPergA16, RespCertaPergA17, RespCertaPergA18, RespCertaPergA19, RespCertaPergA20,
				              RespCertaPergA21, RespCertaPergA22, RespCertaPergA23, RespCertaPergA24, RespCertaPergA25
							};
	
	int lista_sorteadasA[25] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	// Realizando A��o Aleat�ria: 
	
	int perguntasGeradas = 0;
	int RespUsuarioPergA1 = 0;

	while (perguntasGeradas < 5){

		srand(time(NULL));
		
		int RandomPergA = rand() % 25;
		
		* RANDOM = RandomPergA;
		
		if (lista_sorteadasA[RandomPergA] == 0){
		
			*pergunta 		= lista_perguntasA[RandomPergA];
			*alternativa1	= lista_altA[RandomPergA][0];
			*alternativa2	= lista_altA[RandomPergA][1];
			*alternativa3	= lista_altA[RandomPergA][2];
			*alternativa4	= lista_altA[RandomPergA][3];

			RespUsuarioPergA1 = telaPerguntas2(saldoCarteira, saldoAcertar, saldoErrar, *pergunta, *alternativa1, *alternativa2, *alternativa3, *alternativa4);
//			    				 telaPerguntas(&saldoCarteira, &saldoAcertar, &saldoErrar, pergunta, alternativa1, alternativa2, alternativa3, alternativa4);
	
	
			if (RespUsuarioPergA1 == lista_respA[RandomPergA]){
                
				saldoCarteira	= saldoCarteira + 1000;
				saldoAcertar	= saldoAcertar + 1000;
				saldoErrar		= saldoErrar + 4;

                lista_sorteadasA[RandomPergA] = 1; //ALTERA-SE O VALOR DE 0 PARA 1: IMPEDE QUE A PERGUNTA CAIA EM RANDOM NOVAMENTE.
        
                perguntasGeradas = perguntasGeradas + 1;

                printf ("                    PARAB�NS!!! VOC� ACERTOU...\n");

				scanf("x");
				getchar();
				system("CLS");
				
			} else {
				
				printf ("                    ERROU...\n");
                printf ("RESPOSTA CORRETA: %d\n", lista_respA[RandomPergA]);
				printf ("RESPOSTA DO USU�RIO: %d\n", RespUsuarioPergA1);
				
				printf ("CARTEIRA: %d\n", saldoCarteira);
				printf ("ACERTAR : %d\n", saldoAcertar);
				printf ("ERRAR   : %d\n", saldoErrar);
			    
			    saldoCarteira	= saldoCarteira + 1000;
				saldoAcertar	= saldoAcertar + 1000;
				saldoErrar		= saldoErrar + 1000;
				
				printf ("CARTEIRA: %d\n", saldoCarteira);
				printf ("ACERTAR : %d\n", saldoAcertar);
				printf ("ERRAR   : %d\n", saldoErrar);
				
				scanf("x");
				getchar();
				system("CLS");
				exit(1);
			}	            			
			
		} 
		
//		else {
//
//			*pergunta 		= "";
//			*alternativa1	= "";
//			*alternativa2	= "";
//			*alternativa3	= "";
//			*alternativa4	= "";
//		}
	}

	return 0;	
}
// 

