#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>
#include "BD_Perguntas_B.h"

// Gera as Perguntas e Respostas Aleat�rias:
void geraPerguntaB(char ** pergunta, char ** alternativa1, char ** alternativa2, char ** alternativa3, char ** alternativa4){

	char pergB1[200] = "UMA PESSOA CLEPTOMAN�ACA �:";
	char altB1_1[100] = "COLECIONADORA";
	char altB1_2[100] = "DECORADORA";
	char altB1_3[100] = "M�DICA";
	char altB1_4[100] = "DOENTE";
	int respCertaPergB1 = 4;
	int pergSorteada_B1 = 0;
				
	char pergB2[200] = "DE ONDE ERA NATURAL O PINTOR PICASSO?";
	char altB2_1[100] = "ESPANHA";
	char altB2_2[100] = "HOLANDA";
	char altB2_3[100] = "FRAN�A";
	char altB2_4[100] = "B�LGICA";
	int respCertaPergB2 = 1;
	int pergSorteada_B2 = 0;
				
	char pergB3[200] = "QUE IMPERADOR TOCOU FOGO EM ROMA?";
	char altB3_1[100] = "C�SAR";
	char altB3_2[100] = "NERO";
	char altB3_3[100] = "BRUTUS";
	char altB3_4[100] = "CAL�GULA";
	int respCertaPergB3 = 2;
	int pergSorteada_B3 = 0;
				
	char pergB4[200] = "EM QUAL EST�DIO, PEL� MARCOU SEU MIL�SIMO GOL?";
	char altB4_1[100] = "MORUMBI";
	char altB4_2[100] = "PACAEMBU";
	char altB4_3[100] = "MARACAN�";
	char altB4_4[100] = "MINEIR�O";
	int respCertaPergB4 = 3;
	int pergSorteada_B4 = 0;
				
	char pergB5[200] = "O QUE � UM OBO� ?";
	char altB5_1[100] = "VULC�O";
	char altB5_2[100] = "COMIDA";
	char altB5_3[100] = "INSTRUMENTO";
	char altB5_4[100] = "TRIBO";
	int respCertaPergB5 = 3;
	int pergSorteada_B5 = 0;
				
	char pergB6[200] = "COMO ERAM CHAMADOS OS PILOTOS SUICIDAS DA SEGUNDA GUERRA?";
	char altB6_1[100] = "KAMIKAZES";
	char altB6_2[100] = "SASHIMIS";
	char altB6_3[100] = "HARA-KIRIS";
	char altB6_4[100] = "ARIGAT�S";
	int respCertaPergB6 = 1;
	int pergSorteada_B6 = 0;
				
	char pergB7[200] = "O COLISEU � UM MONUMENTO HIST�RICO DE QUE CIDADE EUROP�IA?";
	char altB7_1[100] = "PARIS";
	char altB7_2[100] = "COPENHAGUE";
	char altB7_3[100] = "ROMA";
	char altB7_4[100] = "LONDRES";
	int respCertaPergB7 = 3;
	int pergSorteada_B7 = 0;
				
	char pergB8[200] = "QUEM FOI ELEITO PRESIDENTE DA �FRICA DO SUL EM 1994?";
	char altB8_1[100] = "NELSON PIQUET";
	char altB8_2[100] = "N. MANDELA";
	char altB8_3[100] = "NELSON EDDY";
	char altB8_4[100] = "JOHN NELSON";
	int respCertaPergB8 = 2;
	int pergSorteada_B8 = 0;
				
	char pergB9[200] = "QUANTOS KILATES TEM O OURO PURO?";
	char altB9_1[100] = "18";
	char altB9_2[100] = "20";
	char altB9_3[100] = "24";
	char altB9_4[100] = "30";
	int respCertaPergB9 = 3;
	int pergSorteada_B9 = 0;
		 			
	char pergB10[200] = "QUAL A SIGLA DA ORGANIZA��O DAS NA��ES UNIDAS?";
	char altB10_1[100] = "ONU";
	char altB10_2[100] = "FMI";
	char altB10_3[100] = "CIA";
	char altB10_4[100] = "INPS";
	int respCertaPergB10 = 1;
	int pergSorteada_B10 = 0;
				
	char pergB11[200] = "QUAL A MOEDA OFICIAL DA ALEMANHA?";
	char altB11_1[100] = "LIRA";
	char altB11_2[100] = "MARCO";
	char altB11_3[100] = "PECETA";
	char altB11_4[100] = "IENE";
	int respCertaPergB11 = 2;
	int pergSorteada_B11 = 0;
	 			
	char pergB12[200] = "QUE PA�S EUROPEU TEM COMO ATRA��O A TOURADA ?";
	char altB12_1[100] = "ESPANHA";
	char altB12_2[100] = "IT�LIA";
	char altB12_3[100] = "FRAN�A";
	char altB12_4[100] = "ALEMANHA";
	int respCertaPergB12 = 1;
	int pergSorteada_B12 = 0;
	
	char pergB13[200] = "QUE HUMORISTA, FALECIDO EM 99, FOI BATERISTA DE RAUL SEIXAS?";
	char altB13_1[100] = "LILICO";
	char altB13_2[100] = "RONI C�CEGAS";
	char altB13_3[100] = "GRANDE OTELO";
	char altB13_4[100] = "MAZZAROPI";
	int respCertaPergB13 = 2;
	int pergSorteada_B13 = 0;
	 			
	char pergB14[200] = "O QU� OS FILATELISTAS COLECIONAM?";
	char altB14_1[100] = "QUADROS";
	char altB14_2[100] = "MOEDAS";
	char altB14_3[100] = "SELOS";
	char altB14_4[100] = "FIGURINHAS";
	int respCertaPergB14 = 3;
	int pergSorteada_B14 = 0;
	
	char pergB15[200] = "EM QUE CIDADE EST� LOCALIZADA A FAMOSA 'PRA�A VERMELHA'?";
	char altB15_1[100] = "MOSCOU";
	char altB15_2[100] = "BERLIM";
	char altB15_3[100] = "PARIS";
	char altB15_4[100] = "ROMA";
	int respCertaPergB15 = 1;
	int pergSorteada_B15 = 0;
				
	char pergB16[200] = "QUANDO COME�OU E TERMINOU A PRIMEIRA GUERRA MUNDIAL?";
	char altB16_1[100] = "1914-1918";
	char altB16_2[100] = "1902-1908";
	char altB16_3[100] = "1920-1930";
	char altB16_4[100] = "1912-1915";
	int respCertaPergB16 = 1;
	int pergSorteada_B16 = 0;
				
	char pergB17[200] = "QUEM INTRODUZIU O FUTEBOL NO BRASIL?";
	char altB17_1[100] = "PEL�";
	char altB17_2[100] = "JO�O HAVELANGE";
	char altB17_3[100] = "CHARLES MILLER";
	char altB17_4[100] = "PAULO MACHADO";
	int respCertaPergB17 = 3;
	int pergSorteada_B17 = 0;
				
	char pergB18[200] = "QUEM LEVA O SANGUE DO CORA��O PARA O CORPO?";
	char altB18_1[100] = "VEIAS";
	char altB18_2[100] = "M�SCULOS";
	char altB18_3[100] = "ART�RIAS";
	char altB18_4[100] = "OSSOS";
	int respCertaPergB18 = 3;
	int pergSorteada_B18 = 0;
	
	char pergB19[200] = "QUEM DISSE A FRASE: ' VIM, VI E VENCI ' NO ANO 47 ANTES DE CRISTO?";
	char altB19_1[100] = "J�LIO C�SAR";
	char altB19_2[100] = "CAL�GULA";
	char altB19_3[100] = "NERO";
	char altB19_4[100] = "MARCO ANTONIO";
	int respCertaPergB19 = 1;
	int pergSorteada_B19 = 0;
				
	char pergB20[200] = "QUAL � O PA�S QUE PARTICIPOU DE TODAS AS COPAS DO MUNDO DE FUTEBOL?";
	char altB20_1[100] = "IT�LIA";
	char altB20_2[100] = "URUGUAI";
	char altB20_3[100] = "ARGENTINA";
	char altB20_4[100] = "BRASIL";
	int respCertaPergB20 = 4;
	int pergSorteada_B20 = 0;
							
	char pergB21[200] = "QUAIS S�O OS NAIPES VERMELHOS DO BARALHO?";
	char altB21_1[100] = "OUROS E COPAS";
	char altB21_2[100] = "COPAS E PAUS";
	char altB21_3[100] = "PAUS E OUROS";
	char altB21_4[100] = "ESPADAS E PAUS";
	int respCertaPergB21 = 1;
	int pergSorteada_B21 = 0;
				
	char pergB22[200] = "QUAL O NOME VERDADEIRO DO BATMAN?";
	char altB22_1[100] = "BRUCE WAYNE";
	char altB22_2[100] = "BRUCE BANER";
	char altB22_3[100] = "CLARK KENT";
	char altB22_4[100] = "LEX LUTOR";
	int respCertaPergB22 = 1;
	int pergSorteada_B22 = 0;
					 		
	char pergB23[200] = "QUAL � A CAPITAL DO IRAQUE?";
	char altB23_1[100] = "BEL�M";
	char altB23_2[100] = "BAGD�";
	char altB23_3[100] = "BEIRUTE";
	char altB23_4[100] = "BUDAPESTE";
	int respCertaPergB23 = 2;
	int pergSorteada_B23 = 0;
				
	char pergB24[200] = "EM QUE PA�S FICA A GRANDE MURALHA CONSTRU�DA PELO HOMEM ?";
	char altB24_1[100] = "JAP�O";
	char altB24_2[100] = "CHINA";
	char altB24_3[100] = "AFEGANIST�O";
	char altB24_4[100] = "�NDIA";
	int respCertaPergB24 = 2;
	int pergSorteada_B24 = 0;
				
	char pergB25[200] = "EM QUAL CIDADE DO JAP�O FOI LAN�ADA A PRIMEIRA BOMBA AT�MICA?";
	char altB25_1[100] = "T�KIO";
	char altB25_2[100] = "NAGASAKI";
	char altB25_3[100] = "OSAKA";
	char altB25_4[100] = "HIROSHIMA";
	int respCertaPergB25 = 4;
	int pergSorteada_B25 = 0;
	
	char *lista_perguntasB[25] = {
									pergB1, pergB2, pergB3, pergB4, pergB5,
				                    pergB6, pergB7, pergB8, pergB9, pergB10,
				                    pergB11, pergB12, pergB13, pergB14, pergB15,
				                    pergB16, pergB17, pergB18, pergB19, pergB20,
				                    pergB21, pergB22, pergB23, pergB24, pergB25
								 };
	
	char *lista_altB[25][4] =	{
									{altB1_1, altB1_2, altB1_3, altB1_4},
									{altB2_1, altB2_2, altB2_3, altB2_4},
									{altB3_1, altB3_2, altB3_3, altB3_4},
									{altB4_1, altB4_2, altB4_3, altB4_4},
									{altB5_1, altB5_2, altB5_3, altB5_4},
									{altB6_1, altB6_2, altB6_3, altB6_4},
									{altB7_1, altB7_2, altB7_3, altB7_4},
									{altB8_1, altB8_2, altB8_3, altB8_4},
									{altB9_1, altB9_2, altB9_3, altB9_4}, 
									{altB10_1, altB10_2, altB10_3, altB10_4},
									{altB11_1, altB11_2, altB11_3, altB11_4},
									{altB12_1, altB12_2, altB12_3, altB12_4},
									{altB13_1, altB13_2, altB13_3, altB13_4},
									{altB14_1, altB14_2, altB14_3, altB14_4},
									{altB15_1, altB15_2, altB15_3, altB15_4},
									{altB16_1, altB16_2, altB16_3, altB16_4},
									{altB17_1, altB17_2, altB17_3, altB17_4},
									{altB18_1, altB18_2, altB18_3, altB18_4},
									{altB19_1, altB19_2, altB19_3, altB19_4},
									{altB20_1, altB20_2, altB20_3, altB20_4},
									{altB21_1, altB21_2, altB21_3, altB21_4},
									{altB22_1, altB22_2, altB22_3, altB22_4},
									{altB23_1, altB23_2, altB23_3, altB23_4},
									{altB24_1, altB24_2, altB24_3, altB24_4},
									{altB25_1, altB25_2, altB25_3, altB25_4}
								};
	
	int *lista_respB[25] = {
			              respCertaPergB1, respCertaPergB2, respCertaPergB3, respCertaPergB4, respCertaPergB5,
			              respCertaPergB6, respCertaPergB7, respCertaPergB8, respCertaPergB9, respCertaPergB10,
			              respCertaPergB11, respCertaPergB12, respCertaPergB13, respCertaPergB14, respCertaPergB15,
			              respCertaPergB16, respCertaPergB17, respCertaPergB18, respCertaPergB19, respCertaPergB20,
			              respCertaPergB21, respCertaPergB22, respCertaPergB23, respCertaPergB24, respCertaPergB25
						};
	
	int lista_sorteadasB[25] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	// Realizando A��o Aleat�ria: 
	
	int perguntasGeradas = 0;
	
	while (perguntasGeradas < 1){

		srand(time(NULL));
		
		int RandomPergB = rand() % 25;
		
		if (lista_sorteadasB[RandomPergB] == 0){
		
			*pergunta 		= lista_perguntasB[RandomPergB];
			*alternativa1	= lista_altB[RandomPergB][0];
			*alternativa2	= lista_altB[RandomPergB][1];
			*alternativa3	= lista_altB[RandomPergB][2];
			*alternativa4	= lista_altB[RandomPergB][3];
			
			lista_sorteadasB[RandomPergB] = 1;
			
			perguntasGeradas =+1;
		}

	}
	
}
// 

