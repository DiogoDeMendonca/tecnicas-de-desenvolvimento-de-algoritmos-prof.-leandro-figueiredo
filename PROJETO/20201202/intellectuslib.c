#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>
#include "intellectuslib.h"

// Centraliza Tela:
void centralizaTela(){
	HWND consoleWindow = GetConsoleWindow();

	int x = GetSystemMetrics(SM_CXSCREEN);
	int y = GetSystemMetrics(SM_CYSCREEN);

	SetWindowPos( consoleWindow, 0, x/4, y/4, 0, 0, SWP_NOSIZE | SWP_NOZORDER );
}

// Exibe Tela Inicial de Carregamento:
void telaInicial(){
	
	int n		= 0;
	char contProgresso[100] = {0};	

	system("color 0F");
		
		for ( n = 0 ; n < 10 ; n++ ){
			printf("\n");
		}
		
		printf ("                              -------------------                              \n");
		printf ("                              *** INTELLECTUS ***                              \n");
		printf ("                              -------------------                              \n");
		
		for ( n = 0 ; n < 10 ; n++ ){
			printf("\n");
		}

		printf (" Carregando: ");

		setlocale(LC_ALL, "C");
		
		for( n = 0 ; n < 11 ; n++ ) {			
		
				contProgresso[n] 	= 176;
				
				printf("%s", contProgresso);
				
				Sleep(500);
		}

}

// Estrutura e Exibe o Cabe�alho [ T�tulo e SubT�tulo ]:
void cabecalho(){
	
	setlocale(LC_ALL, "C");

	int n = 0;

	// Borda Superior:
	printf(" ");
	for ( n = 0 ; n < 78 ; n++ ){
		printf("%c", 220);
	}
	
	printf("\n");

	for ( n = 0 ; n < 1 ; n++ ){
		printf(" %c                                                                            %c\n", 186, 186);
	}

	printf(" %c                            ..:: INTELLECTUS ::..                           %c\n", 186, 186);
	printf(" %c                         Desafie Seus Conhecimentos                         %c\n", 186, 186);

	for ( n = 0 ; n < 1 ; n++ ){
		printf(" %c                                                                            %c\n", 186, 186);
	}

	// Borda Inferior:
	printf(" ");			
	for ( n = 0 ; n < 78 ; n++ ){
		printf("%c", 223);
	}			
	printf("\n");
	
	// Linha Superior COMPLETA:			
	for ( n = 0 ; n < 80 ; n++ ){
			printf("%c", 205);
	
	}
	
}

// Estrutura e Exibe o Cabe�alho [ T�tulo ]:
void cabecalhoSimples(){
	
	setlocale(LC_ALL, "C");

	int n = 0;

	// Borda Superior:
	printf(" ");
	for ( n = 0 ; n < 78 ; n++ ){
		printf("%c", 220);
	}
	
	printf("\n");

	for ( n = 0 ; n < 1 ; n++ ){
		printf(" %c                                                                            %c\n", 186, 186);
	}

	printf(" %c                            ..:: INTELLECTUS ::..                           %c\n", 186, 186);

	for ( n = 0 ; n < 1 ; n++ ){
		printf(" %c                                                                            %c\n", 186, 186);
	}

	// Borda Inferior:
	printf(" ");			
	for ( n = 0 ; n < 78 ; n++ ){
		printf("%c", 223);
	}			
	printf("\n");
	
//	// Linha Superior COMPLETA:			
//	for ( n = 0 ; n < 80 ; n++ ){
//			printf("%c", 205);
//	
//	}
	
}

// Desloca o curso para determinada coordenada:
void GoToXY(int posX, int posY, char string[100]){
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD coordenadas = { posX, posY };
    SetConsoleCursorPosition(hStdout, coordenadas);
    printf("%s", string);    
}

// Desenha o banner da �rea Principal:
void banner(){
		setlocale(LC_ALL, "C");
		printf("                                                                               \n"); // 01
		printf("                                                                               \n"); // 02
		
		printf("   %c%c%c%c%c %c%c  %c%c %c%c%c%c%c %c%c%c%c%c%c %c%c     %c%c     %c%c%c%c%c%c   %c%c%c%c  %c%c%c%c%c %c%c  %c%c  %c%c%c%c   \n", 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219 ); // 03 -39
		printf("     %c   %c%c%c %c%c   %c   %c%c     %c%c     %c%c     %c%c      %c%c  %c%c   %c   %c%c  %c%c %c%c  %c   \n", 219, 219, 219, 220, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219); // 04 - 26
		printf("     %c   %c%c%c%c%c%c   %c   %c%c     %c%c     %c%c     %c%c      %c%c       %c   %c%c  %c%c %c%c      \n", 219, 219, 219, 219, 220, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219); // 05 - 23
		printf("     %c   %c%c%c%c%c%c   %c   %c%c%c%c%c  %c%c     %c%c     %c%c%c%c%c   %c%c       %c   %c%c  %c%c  %c%c%c%c   \n", 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219,219); // 06 - 28
		printf("     %c   %c%c%c%c%c%c   %c   %c%c     %c%c     %c%c     %c%c      %c%c       %c   %c%c  %c%c     %c%c  \n", 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219); // 07 - 23
		printf("     %c   %c%c %c%c%c   %c   %c%c     %c%c     %c%c     %c%c      %c%c  %c%c   %c   %c%c  %c%c %c%c  %c%c  \n", 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219); // 08 - 26
		printf("   %c%c%c%c%c %c%c  %c%c   %c   %c%c%c%c%c%c %c%c%c%c%c%c %c%c%c%c%c%c %c%c%c%c%c%c   %c%c%c%c    %c    %c%c%c%c   %c%c%c%c   \n", 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219, 219); // 09 - 41
		
		printf("                                                                               \n"); // 10
		printf("                                                                               \n"); // 11
	}

// Estrutura as Coordenadas do Menu Horizontal:
void menu(){
	
	GoToXY( 9, 21, "Jogar");
	GoToXY(21, 21, "Conquistas");
	GoToXY(38, 21, "Op��es");
	GoToXY(51, 21, "Cr�ditos");
	GoToXY(66, 21, "Sair        ");
}

// Quebra linha para perguntas com mais de 70 caracteres:
void quebraLinha(char * pergunta){

	if(strlen(pergunta) > 70){

		int i = 0;
		int n = 0;
				
		char linha1[100], linha2[100];
		
			for( i = 0 ; (!(i > 60  && i < 70 && pergunta[i] == ' ')); i++){
				
				linha1[i] = pergunta[i];

			}
		
			printf("\t%s\n", linha1);
			
			for( i, n ; i < strlen(pergunta) ; i++, n++ ){
				
				linha2[n] = pergunta[i];
			}			
			
			printf("\t%s\n", linha2);
	}
	
}

// Calcula e Exibe a Pontua��o do Jogador: 
void pontuacao(int *saldoCarteira, int *saldoAcertar, int *saldoErrar){
	
		printf("\t %c CARTEIRA %c \t\t  %c  ACERTAR  %c \t   %c   ERRAR   %c\n", 186, 186, 186, 186, 186, 186);
		printf("\t %c%9d %c \t\t  %c%9d  %c \t   %c%9d  %c\n", 186, saldoCarteira, 186, 186, saldoAcertar, 186, 186, saldoErrar, 186);
}

// Calcula e Exibe o Tempo Regressivo de Resposta do Jogador:
int temporizador(){
	
	int T 			=   0; // Tempo da Barra De Progresso.
	int t 			=   0; // Tempo Num�rico Regressivo.
	char tempo[100]	= {0}; // Barra De Progresso: Elementro Gr�fico.
	char opcao;
	int opcaoint = 0;
	
	for( T = 0, t = 60 ; T < 60, t >= 0; T++, t--) {			

		if(kbhit()) {
			
//			scanf("%d", &opcao);
//			return opcao;
//			break;
	        COD_PTBR;
			//printf("\nOp��o Digitada: [ %c ]\n\n", getch());
			

			opcao = getch();
			
			//opcaoint = atoi(opcao);
//			
			printf("\nOp��o Digitada: INT [ %d ]\n\n", opcaoint);
			printf("\nOp��o Digitada: CHAR[ %c ]\n\n", opcao);
			
			return opcao;
			
//			//opcao = printf("\nOp��o Digitada: Get[ %d ]\n\n", getch());
//			opcaoint = atoi(opcao);
//			
//			printf("\nOp��o Digitada: OP[ %d ]\n\n", opcaoint);
//			system("PAUSE");
//			return opcao;
//			break;
//	        getchar();
//			printf("\n");
//			return getch();
//	        break;
	    }

		tempo[T] 	= 176;
		
		printf("  %2ds   %s\r", t, tempo);

		Sleep(500);
	}
}

// Estrutura e Exibe o Rodap�:
void rodape(){

	int n = 0;

	// Linha Inferior COMPLETA:

		setlocale(LC_ALL, "C");
		//printf(" ");
		for ( n = 0 ; n < 80 ; n++ ){
			printf("%c", 205);
		}

		// Inicio Retangulo:
		
	// Borda Superior:
	printf(" ");
	for ( n = 0 ; n < 78 ; n++ ){
		printf("%c", 220);
	}
	
	printf("\n");

	for ( n = 0 ; n < 2 ; n++ ){
		printf(" %c                                                                            %c\n", 186, 186);
	}
			
	setlocale(LC_ALL, "C");
	//printf("%c\n", 186);
				
	for ( n = 0 ; n < 1 ; n++ ){
		printf(" %c                                                                            %c\n", 186, 186);
	}
	
	// Borda Inferior:
	printf(" ");			
	for ( n = 0 ; n < 78 ; n++ ){
		printf("%c", 223);
	}		
	
	printf("\n");
	
}

// Estrutura e Exibe o Rodap� de Perguntas:
void rodapePerguntas(){

	int n = 0;

	// Linha Inferior COMPLETA:

		setlocale(LC_ALL, "C");
		//printf(" ");
		for ( n = 0 ; n < 80 ; n++ ){
			printf("%c", 205);
		}

		// Inicio Retangulo:
		
	// Borda Superior:
	printf(" ");
	for ( n = 0 ; n < 78 ; n++ ){
		printf("%c", 220);
	}
	
	printf("\n");

	for ( n = 0 ; n < 1 ; n++ ){
		printf(" %c                                                                            %c\n", 186, 186);
	}
			
	setlocale(LC_ALL, "C");
	printf(" %c                    >>> Digite a Alternativa Correta <<<                    %c\n", 186, 186);
				
	for ( n = 0 ; n < 1 ; n++ ){
		printf(" %c                                                                            %c\n", 186, 186);
	}
	
	// Borda Inferior:
	printf(" ");			
	for ( n = 0 ; n < 78 ; n++ ){
		printf("%c", 223);
	}		
	
	printf("\n");
	
	printf("\t%c                           TEMPO                           %c\n", 31, 31);

}

// Estrutura e Exibe o Rodap� da Tela de Nome do Jogador:
void rodapeJogador(){

	int n = 0;

	// Linha Inferior COMPLETA:

		setlocale(LC_ALL, "C");
		//printf(" ");
		for ( n = 0 ; n < 80 ; n++ ){
			printf("%c", 205);
		}

		// Inicio Retangulo:
		
	// Borda Superior:
	printf(" ");
	for ( n = 0 ; n < 78 ; n++ ){
		printf("%c", 220);
	}
	
	printf("\n");

	for ( n = 0 ; n < 1 ; n++ ){
		printf(" %c                                                                            %c\n", 186, 186);
	}
			
	setlocale(LC_ALL, "C");
	printf(" %c              >>> Estamos Conhecendo o Nosso Participante <<<               %c\n", 186, 186);
				
	for ( n = 0 ; n < 1 ; n++ ){
		printf(" %c                                                                            %c\n", 186, 186);
	}
	
	// Borda Inferior:
	printf(" ");			
	for ( n = 0 ; n < 78 ; n++ ){
		printf("%c", 223);
	}		

}

// Manipula Sele��o do Menu Principal:
int selecionaMenu(){

	char a 		= '0';
	int opMenu	=   0;
	int posX	=   7;
	int posY 	=  21;

	do {	
	
		setlocale(LC_ALL, "portuguese");
		menu();

		GoToXY( posX, posY, ">");
		
		a = toupper(getch());
		
		switch(a){
		
			case 77 :
								
				// Jogar:
				if ( posX == 7 ){
                    GoToXY( posX, posY, " ");
                    posX = 19;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(500, 100);
                } else 
                
                // Conquistas:
                if ( posX == 19 ){
                    GoToXY( posX, posY, " ");
                    posX = 36;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(500, 100);
                } else 
                
                // Op��es:
                if ( posX == 36 ){
                    GoToXY( posX, posY, " ");
                    posX = 49;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(500, 100);
                } else
                
                // Cr�ditos:
                if ( posX == 49 ){
                    GoToXY( posX, posY, " ");
                    posX = 64;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(500, 100);
                } else 
                
                // Sair:
                if ( posX == 64 ){
                    GoToXY( posX, posY, " ");
                    posX = 7;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(500, 100);
                }
        	break;
        	
        	case 75 :
        		
				// Sair:
                if ( posX == 64 ){
                    GoToXY( posX, posY, " ");
                    posX = 49;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(400, 100);
                } else
                
                // Cr�ditos:
                if ( posX == 49 ){
                    GoToXY( posX, posY, " ");
                    posX = 36;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(400, 100);
                } else
                
                // Op��es:
                if ( posX == 36 ){
                    GoToXY( posX, posY, " ");
                    posX = 19;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(400, 100);
                } else
                
                // Conquistas:
                if ( posX == 19 ){
                    GoToXY( posX, posY, " ");
                    posX = 7;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(400, 100);
                } else 
                
                // Jogar:
				if ( posX == 7 ){
                    GoToXY( posX, posY, " ");
                    posX = 64;
                    					
					GoToXY( posX, posY, ">");
					             
                    _beep(400, 100);
                }
			break; 
			
			case 13:

				// Jogar:
				if ( posX == 7 ){
					system("CLS");
					//printf("JOGAR");
					opMenu = 1;
					return opMenu;
				}
                       
                // Conquistas:
                if ( posX == 19 ){
					system("CLS");
					//printf("CONQUISTAS");
					opMenu = 2;
					return opMenu;
                } else 
                
                // Op��es:
                if ( posX == 36 ){
					system("CLS");
					//printf("OP��ES");
					opMenu = 3;
					return opMenu;
                } else
                
                // Cr�ditos:
                if ( posX == 49 ){
					system("CLS");
					//printf("CR�DITOS");
					opMenu = 4;
					return opMenu;
                } else
                
                // Sair:
				if ( posX == 64 ){
					system("CLS");
                    exit(0);
            	}
			break;
//===========================================================						
		//default:	
		}
	} while (1);
}

// Estrutura e Exibe a Tela de Recebimento dos Dados do Jogador:
int telaJogador(char ** pJogador){
	
	//system("color 0F");
	
	int n = 0;

	// ################# C A B E C A L H O #######
	cabecalho();

	// ################# C O R P O ###############
	
	for ( n = 0 ; n < 11 ; n++  ){
		printf("\n");
	}
	
		// Realizado por GoToXY;
	
	// ################# R O D A P E #############
	rodapeJogador();
	
	GoToXY(10, 10, "...DIGITE SEU NOME: >>> ");
	scanf("%[ -~]", *pJogador);
	getchar();
	system("CLS");
	
	return 0;
}

// Estrutura e Exibe a Tela Principal:
int telaPrincipal(){
	
	//system("color 0F");
	
	int opMenu 	=   0;

	// ################# C A B E C A L H O #######
	cabecalho();

	// ################# C O R P O ###############
	banner();
	
	// ################# R O D A P E #############
	rodape();
	
	// ################# M E N U #################
	opMenu = selecionaMenu();
	
	return opMenu;
}

//// Estrutura e Exibe a Tela de Perguntas:
//int telaPerguntas(int saldoCarteira, int saldoAcertar, int saldoErrar, char *Perg, char *alt_1, char *alt_2, char *alt_3, char *alt_4){
//	
//	//system("color 0F");
//	
//	SetConsoleTitle("..:: INTELLECTUS: Perguntas ::..");
//	
//	int n = 0;
////	int Carteira	= saldoCarteira;
////	int Acertar		= saldoAcertar;
////	int Errar		= saldoAcertar;
//
//	// ################# C A B E C A L H O #######
//	cabecalhoSimples();
//
//	pontuacao(saldoCarteira, saldoAcertar, saldoErrar);
//	
//	// Linha Superior COMPLETA:			
//	for ( n = 0 ; n < 80 ; n++ ){
//			printf("%c", 205);
//	
//	}
//
//	// ################# C O R P O ###############
//	
//		/* >>> FUN��O PERGUNTAS <<< */  
//		
//	// PERGUNTA:
//	//quebraLinha(Perg);
//	setlocale(LC_ALL, "portuguese");
//	printf("\n");
//	printf("\t%s", Perg);
//
//		/* >>> FUN��O ALTERNATIVAS <<< */
//
//	// ALTERNATIVAS:
//	printf("\n");
//	printf("\n");
//	printf("\t1: %s\n", alt_1);
//	printf("\t2: %s\n", alt_2);
//	printf("\t3: %s\n", alt_3);
//	printf("\t4: %s\n", alt_4);
////	printf("\n");
//	
//	// ################# R O D A P E #############
//	rodapePerguntas();
//	
//	// TEMPO => Elemento Gr�fico Regressivo:
//	temporizador();
//
//	return 0;	
//}

// Estrutura e Exibe a Tela de Perguntas:
void telaPerguntas2(int *saldoCarteira, int *saldoAcertar, int *saldoErrar, char *Perg, char *alt_1, char *alt_2, char *alt_3, char *alt_4){
	
	//system("color 0F");
	
	SetConsoleTitle("..:: INTELLECTUS: Perguntas ::..");
	
	int n = 0;
	int resposta = 0;

	// ################# C A B E C A L H O #######
	cabecalhoSimples();

	pontuacao(saldoCarteira, saldoAcertar, saldoErrar);
	
	// Linha Superior COMPLETA:			
	for ( n = 0 ; n < 80 ; n++ ){
			printf("%c", 205);
	
	}

	// ################# C O R P O ###############
	
		/* >>> FUN��O PERGUNTAS <<< */  
		
	// PERGUNTA:
	//quebraLinha(Perg);
	setlocale(LC_ALL, "portuguese");
	printf("\n");
	printf("\t%s", Perg);

		/* >>> FUN��O ALTERNATIVAS <<< */

	// ALTERNATIVAS:
	printf("\n");
	printf("\n");
	printf("\t1: %s\n", alt_1);
	printf("\t2: %s\n", alt_2);
	printf("\t3: %s\n", alt_3);
	printf("\t4: %s\n", alt_4);
//	printf("\n");
	
	// ################# R O D A P E #############
	rodapePerguntas();
	
	// TEMPO => Elemento Gr�fico Regressivo:
	temporizador();
//	
//	printf("\nTEMPORIZADOR : %d\n", resposta);
//	
//	return resposta;
}
 

//

