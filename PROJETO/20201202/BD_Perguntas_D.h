/* BD_Perguntas_D */

#ifndef BD_Perguntas_D
#define BD_Perguntas_D

// Gera as Perguntas e Respostas Aleatórias - NIVEL D:
void geraPerguntaD(char ** pergunta, char ** alternativa1, char ** alternativa2, char ** alternativa3, char ** alternativa4);

#endif /* BD_Perguntas_D */

