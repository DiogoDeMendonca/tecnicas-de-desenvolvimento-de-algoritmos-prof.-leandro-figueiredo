/* BD_Perguntas_B */

#ifndef BD_Perguntas_B
#define BD_Perguntas_B

// Gera as Perguntas e Respostas Aleatórias - NIVEL B:
void geraPerguntaB(char ** pergunta, char ** alternativa1, char ** alternativa2, char ** alternativa3, char ** alternativa4);

#endif /* BD_Perguntas_B */

