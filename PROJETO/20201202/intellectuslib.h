/* INTELLECTUSLIB_H */

#ifndef INTELLECTUSLIB_H
#define INTELLECTUSLIB_H

	/* CONSTANTES */

// Define os padr�o ASCII compat�vel com a l�ngua Portuguesa / Linguagem Padr�o C:
#define COD_C setlocale(LC_ALL, "C")
#define COD_PTBR setlocale(LC_ALL, "portuguese")

// Centraliza Tela:
void centralizaTela();

// Exibe Tela Inicial de Carregamento:
void telaInicial();

// Estrutura e Exibe o Cabe�alho [ T�tulo e SubT�tulo ]:
void cabecalho();

// Estrutura e Exibe o Cabe�alho [ T�tulo ]:
void cabecalhoSimples();

// Desloca o curso para determinada coordenada:
void GoToXY(int posX, int posY, char string[100]);

// Desenha o banner da �rea Principal:
void banner();

// Estrutura as Coordenadas do Menu Horizontal:
void menu();

// Quebra linha para perguntas com mais de 70 caracteres:
void quebraLinha(char * pergunta);

// Calcula e Exibe a Pontua��o do Jogador: 
void pontuacao();

// Calcula e Exibe o Tempo Regressivo de Resposta do Jogador:
int temporizador();

// Estrutura o Rodap�:
void rodape();

// Estrutura e Exibe o Rodap� de Perguntas:
void rodapePerguntas();

// Estrutura e Exibe o Rodap� da Tela de Nome do Jogador:
void rodapeJogador();

// Manipula Sele��o do Menu Principal:
int selecionaMenu();

// Estrutura e Exibe a Tela de Recebimento dos Dados do Jogador:
int telaJogador();
	
// Estrutura e Exibe a Tela Principal:
int telaPrincipal();

// Estrutura e Exibe a Tela de Perguntas:
int telaPerguntas();

// Estrutura e Exibe a Tela de Perguntas:
void telaPerguntas2();
	
#endif /* INTELLECTUSLIB_H */

