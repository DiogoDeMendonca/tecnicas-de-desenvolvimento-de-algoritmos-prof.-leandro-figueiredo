#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>
#include "intellectuslib.h"
#include "BD_Perguntas_A.h"
#include "BD_Perguntas_B.h"
#include "BD_Perguntas_C.h"
#include "BD_Perguntas_D.h"


int main(){
	
	int opcaoMenu 		= 0;
	int carteira		= 99;
	int acertar			= 20;
	int errar			= 30;
	int *saldoCarteira	= NULL;
	int *saldoAcertar	= NULL;
	int *saldoErrar		= NULL;
	char jogador[20]	= "Anonimo";
	char *pJogador 		= NULL;
	char *pergunta 		= NULL;
	char *alternativa1	= NULL;
	char *alternativa2	= NULL;
	char *alternativa3	= NULL;
	char *alternativa4	= NULL;
	int  *RANDOM	    = NULL;
	
	saldoCarteira = carteira;
	saldoAcertar  = acertar;
	saldoErrar    = errar;
	pJogador      = jogador;
	
	// Recebe a resolu��o da tela e centraliza a exibi��o do programa:
	centralizaTela();
	
	// T�tulo da Janela:
	SetConsoleTitle("..:: INTELLECTUS: Desafie Seus Conhecimentos! ::..");
	
	// Define os padr�o ASCII compat�vel com a l�ngua Portuguesa:
	COD_PTBR;
	
	telaInicial();
		
	system("CLS");

	opcaoMenu = telaPrincipal();
	
	switch(opcaoMenu){
		
		case 1: // Jogar:
			system("CLS");
						
			telaJogador(&pJogador);

			geraPerguntaA(saldoCarteira, saldoAcertar, saldoErrar, &pergunta, &alternativa1, &alternativa2, &alternativa3, &alternativa4, &RANDOM);
//			printf("RANDOM: %d: \n", RANDOM);
//			telaPerguntas(&saldoCarteira, &saldoAcertar, &saldoErrar, pergunta, alternativa1, alternativa2, alternativa3, alternativa4);
			
		break;
		
		case 2: // Conquistas:
			system("CLS");
			printf("> CONQUISTAS");
		break;

		case 3: // Op��es:
        	system("CLS");
			printf("> OP��ES");
		break;
		
		case 4: // Cr�ditos:
			system("CLS");
			printf("> CR�DITOS");
		break;

		case 5: // Sair:
			system("CLS");
            exit(0);
        break;
	}
	
//	geraPerguntaA(&pergunta, &alternativa1, &alternativa2, &alternativa3, &alternativa4, &RANDOM);
//	printf("RANDOM: %d: \n", RANDOM);
//	telaPerguntas(saldoCarteira, saldoAcertar, saldoErrar, pergunta, alternativa1, alternativa2, alternativa3, alternativa4);
	
	
//	geraPerguntaB(&pergunta, &alternativa1, &alternativa2, &alternativa3, &alternativa4);
//	telaPerguntas(saldoCarteira, saldoAcertar, saldoErrar, pergunta, alternativa1, alternativa2, alternativa3, alternativa4);
//	
//	geraPerguntaC(&pergunta, &alternativa1, &alternativa2, &alternativa3, &alternativa4);
//	telaPerguntas(saldoCarteira, saldoAcertar, saldoErrar, pergunta, alternativa1, alternativa2, alternativa3, alternativa4);
//	
//	geraPerguntaD(&pergunta, &alternativa1, &alternativa2, &alternativa3, &alternativa4);
//	telaPerguntas(saldoCarteira, saldoAcertar, saldoErrar, pergunta, alternativa1, alternativa2, alternativa3, alternativa4);

//	telaInicial();
//
//	system("CLS");

//	fflush(stdout);

// ########## F I N A L I Z A D O ##########

//	opcaoMenu = telaPrincipal();
//	
//	printf("Op��o Menu: %d", opcaoMenu);
//	
//	switch(opcaoMenu){
//		
//		case 1: // Jogar:
//			system("CLS");
//			printf("> JOGAR");
//		break;
//		
//		case 2: // Conquistas:
//			system("CLS");
//			printf("> CONQUISTAS");
//		break;
//
//		case 3: // Op��es:
//        	system("CLS");
//			printf("> OP��ES");
//		break;
//		
//		case 4: // Cr�ditos:
//			system("CLS");
//			printf("> CR�DITOS");
//		break;
//
//		case 5: // Sair:
//			system("CLS");
//            exit(0);
//        break;
//	}
//	
	
	printf("\n");
	printf("Saindo...\n\n");
	
	Sleep(1000);

	printf("Saiu...");
	system("pause");

	return 0;	
}
