/* BD_Perguntas_A */

#ifndef BD_Perguntas_A
#define BD_Perguntas_A

// Gera as Perguntas e Respostas Aleatórias - NIVEL A:
int geraPerguntaA(int *saldoCarteira, int *saldoAcertar, int *saldoErrarchar, char ** pergunta, char ** alternativa1, char ** alternativa2, char ** alternativa3, char ** alternativa4, int * RANDOM);

#endif /* BD_Perguntas_A */

