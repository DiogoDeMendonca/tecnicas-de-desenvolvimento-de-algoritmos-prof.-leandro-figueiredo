#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <windows.h>
#include "BD_Perguntas_C.h"

// Gera as Perguntas e Respostas Aleat�rias:
void geraPerguntaC(char ** pergunta, char ** alternativa1, char ** alternativa2, char ** alternativa3, char ** alternativa4){

	char PergC1[200] = "QUEM COMP�S O CONCERTO 'AS QUATROS ESTA��ES'?";
	char altC1_1[100] = "ANTONIO VIVALDI";
	char altC1_2[100] = "ANTONIO CARLOS JOBIM";
	char altC1_3[100] = "RICHARD WAGNER";
	char altC1_4[100] = "RICHARD DREYFUSS";
	int RespCertaPergC1 = 1;
	int pergSorteada_C1 = 0;
			
	char PergC2[200] = "QUEM � O AUTOR DO MANIFESTO COMUNISTA ?";
	char altC2_1[100] = "LENIN";
	char altC2_2[100] = "GORBATCHOV";
	char altC2_3[100] = "KARL MARX";
	char altC2_4[100] = "ALLAN KARDEC";
	int RespCertaPergC2 = 3;
	int pergSorteada_C2 = 0;
				
	char PergC3[200] = "QUAL DESSES PA�SES TEM A CAPITAL COM O MESMO NOME?";
	char altC3_1[100] = "IRAQUE";
	char altC3_2[100] = "LUXEMBURGO";
	char altC3_3[100] = "IUGOSL�VIA";
	char altC3_4[100] = "JORD�NIA";
	int RespCertaPergC3 = 2;
	int pergSorteada_C3 = 0;
				
	char PergC4[200] = "QUAL FLOR � O S�MBOLO NACIONAL DO EGITO E FOI CONSIDERADA SAGRADA NA ANTIGUIDADE?";
	char altC4_1[100] = "ROSA";
	char altC4_2[100] = "L�RIO";
	char altC4_3[100] = "JASMIM";
	char altC4_4[100] = "LOTUS";
	int RespCertaPergC4 = 4;
	int pergSorteada_C4 = 0;
				
	char PergC5[200] = "EM QUE PA�S FOI REALIZADA A PRIMEIRA COPA DO MUNDO DE FUTEBOL?";
	char altC5_1[100] = "PORTUGAL";
	char altC5_2[100] = "URUGUAI";
	char altC5_3[100] = "INGLATERRA";
	char altC5_4[100] = "IT�LIA";
	int RespCertaPergC5 = 2;
	int pergSorteada_C5 = 0;
				
	char PergC6[200] = "QUAL INSTRUMENTO MEDE A ALTITUDE DO SOL E OUTROS CORPOS CELESTES?";
	char altC6_1[100] = "SONAR";
	char altC6_2[100] = "RADAR";
	char altC6_3[100] = "SEXTANTE";
	char altC6_4[100] = "OD�METRO";
	int RespCertaPergC6 = 3;
	int pergSorteada_C6 = 0;
				
	char PergC7[200] = "COM QUANTOS P�ES JESUS CRISTO ALIMENTOU OS 5000?";
	char altC7_1[100] = "DOIS";
	char altC7_2[100] = "TR�S";
	char altC7_3[100] = "QUATRO";
	char altC7_4[100] = "CINCO";
	int RespCertaPergC7 = 4;
	int pergSorteada_C7 = 0;
		 		
	char PergC8[200] = "QUE  PA�S  DA  EUROPA  � CONHECIDO  COMO  PA�SES BAIXOS?";
	char altC8_1[100] = "HOLANDA";
	char altC8_2[100] = "�USTRIA";
	char altC8_3[100] = "B�LGICA";
	char altC8_4[100] = "HUNGRIA";
	int RespCertaPergC8 = 1;
	int pergSorteada_C8 = 0;
				
	char PergC9[200] = "O QUE � ORNITORRINCO?";
	char altC9_1[100] = "VULC�O";
	char altC9_2[100] = "RIO";
	char altC9_3[100] = "LEGUME";
	char altC9_4[100] = "ANIMAL";
	int RespCertaPergC9 = 4;
	int pergSorteada_C9 = 0;
				
	char PergC10[200] = "QUE ANIMAL MITOL�GICO RENASCIA DAS PR�PRIAS CINZAS?";
	char altC10_1[100] = "UM LE�O";
	char altC10_2[100] = "UMA AVE";
	char altC10_3[100] = "UM CAVALO";
	char altC10_4[100] = "UMA SERPENTE";
	int RespCertaPergC10 = 2;
	int pergSorteada_C10 = 0;
	 			
	char PergC11[200] = "QUAL CIDADE O ROBOCOP COMBATE O CRIME?";
	char altC11_1[100] = "NOVA YORK";
	char altC11_2[100] = "DETROIT";
	char altC11_3[100] = "LOS ANGELES";
	char altC11_4[100] = "SAN FRANCISCO";
	int RespCertaPergC11 = 2;
	int pergSorteada_C11 = 0;
				
	char PergC12[200] = "EM QUE ;ANO O BRASIL CONQUISTOU SUA PRIMEIRA COPA DO MUNDO ?";
	char altC12_1[100] = "1962";
	char altC12_2[100] = "1958";
	char altC12_3[100] = "1970";
	char altC12_4[100] = "1994";
	int RespCertaPergC12 = 2;
	int pergSorteada_C12 = 0;
	
	char PergC13[200] = "EM QUAL ESTADO BRASILEIRO ACONTECEU A GUERRA DE CANUDOS ?";
	char altC13_1[100] = "RIO DE JANEIRO";
	char altC13_2[100] = "BAHIA";
	char altC13_3[100] = "PERNAMBUCO";
	char altC13_4[100] = "CEAR�";
	int RespCertaPergC13 = 2;
	int pergSorteada_C13 = 0;
				
	char PergC14[200] = "QUEM LIDEROU A REVOLU��O CONTRA A DITADURA DE FULGENCIO BATISTA EM 1959?";
	char altC14_1[100] = "CHE GUEVARA";
	char altC14_2[100] = "FIDEL CASTRO";
	char altC14_3[100] = "JUAN D. PER�N";
	char altC14_4[100] = "GENERAL PINOCHET";
	int RespCertaPergC14 = 2;
	int pergSorteada_C14 = 0;
	
	char PergC15[200] = "EM QUE ANO O MURO DE BERLIM FOI DERRUBADO ?";
	char altC15_1[100] = "1975";
	char altC15_2[100] = "1984";
	char altC15_3[100] = "1990";
	char altC15_4[100] = "1989";
	int RespCertaPergC15 = 4;
	int pergSorteada_C15 = 0;
				
	char PergC16[200] = "QUEM TEM A INCUMB�NCIA DE ESCOLHER O NOSSO PAPA ?";
	char altC16_1[100] = "C�NEGOS";
	char altC16_2[100] = "BISPOS";
	char altC16_3[100] = "ARCEBISPOS";
	char altC16_4[100] = "CARDEAIS";
	int RespCertaPergC16 = 4;
	int pergSorteada_C16 = 0;
				
	char PergC17[200] = "QUEM ERA O DEUS SUPREMO NA ANTIGA RELIGI�O GREGA?";
	char altC17_1[100] = "MERC�RIO";
	char altC17_2[100] = "ZEUS";
	char altC17_3[100] = "APOLO";
	char altC17_4[100] = "NETUNO";
	int RespCertaPergC17 = 2;
	int pergSorteada_C17 = 0;
				
	char PergC18[200] = "QUAL ERA O DEUS ROMANO DA GERRA?";
	char altC18_1[100] = "MARTE";
	char altC18_2[100] = "J�PITER";
	char altC18_3[100] = "HADES";
	char altC18_4[100] = "NETUNO";
	int RespCertaPergC18 = 1;
	int pergSorteada_C18 = 0;
	
	char PergC19[200] = "EM QUE ASSUNTO UM EN�LOGO � ESPECIALISTA?";
	char altC19_1[100] = "MOEDAS";
	char altC19_2[100] = "VINHOS";
	char altC19_3[100] = "LIVROS";
	char altC19_4[100] = "�NDIOS";
	int RespCertaPergC19 = 2;
	int pergSorteada_C19 = 0;
				
	char PergC20[200] = "COMO REPRESENTAMOS EM ALGARISMOS ROMANOS O N�MERO 500 ?";
	char altC20_1[100] = "C";
	char altC20_2[100] = "M";
	char altC20_3[100] = "D";
	char altC20_4[100] = "L";
	int RespCertaPergC20 = 3;
	int pergSorteada_C20 = 0;
							
	char PergC21[200] = "QUINGENT�SIMO CORRESPONDE A QUAL N�MERO ?";
	char altC21_1[100] = "500";
	char altC21_2[100] = "5000";
	char altC21_3[100] = "50";
	char altC21_4[100] = "5";
	int RespCertaPergC21 = 1;
	int pergSorteada_C21 = 0;
				
	char PergC22[200] = "QUAL A COBRA DO BRASIL SE ALIMENTA DE OUTRA?";
	char altC22_1[100] = "MU�URANA";
	char altC22_2[100] = "CORAL";
	char altC22_3[100] = "CASCAVEL";
	char altC22_4[100] = "JIB�IA";
	int RespCertaPergC22 = 1;
	int pergSorteada_C22 = 0;
		 		
	char PergC23[200] = "EM UMA ARMADURA QUAL PARTE DO CORPO � PROTEGIDO PELO GUANTE?";
	char altC23_1[100] = "PEITO";
	char altC23_2[100] = "M�OS";
	char altC23_3[100] = "ROSTO";
	char altC23_4[100] = "ABDOME";
	int RespCertaPergC23 = 2;
	int pergSorteada_C23 = 0;
				
	char PergC24[200] = "MEDUSAS SIFON�FORAS E CTEN�FORAS S�O CATEGORIAS DE QU�?";
	char altC24_1[100] = "PLANTAS";
	char altC24_2[100] = "FLORES";
	char altC24_3[100] = "MAM�FEROS";
	char altC24_4[100] = "�GUAS-VIVAS";
	int RespCertaPergC24 = 4;
	int pergSorteada_C24 = 0;
		 		
	char PergC25[200] = "QUAL A CI�NCIA QUE ESTUDA A ORIGEM E DESENVOLVIMENTO DAS PALAVRAS?";
	char altC25_1[100] = "ENDOSCOPIA";
	char altC25_2[100] = "GRAFOLOGIA";
	char altC25_3[100] = "RADIOGRAFIA";
	char altC25_4[100] = "ETIMOLOGIA";
	int RespCertaPergC25 = 4;
	int pergSorteada_C25 = 0;
	
	char *lista_perguntasC[25] = {
								PergC1, PergC2, PergC3, PergC4, PergC5,
			                    PergC6, PergC7, PergC8, PergC9, PergC10,
			                    PergC11, PergC12, PergC13, PergC14, PergC15,
			                    PergC16, PergC17, PergC18, PergC19, PergC20,
			                    PergC21, PergC22, PergC23, PergC24, PergC25
								 };
	                    
	char *lista_altC[25][4] =	{
					             {altC1_1, altC1_2, altC1_3, altC1_4},
					             {altC2_1, altC2_2, altC2_3, altC2_4},
					             {altC3_1, altC3_2, altC3_3, altC3_4},
					             {altC4_1, altC4_2, altC4_3, altC4_4},
					             {altC5_1, altC5_2, altC5_3, altC5_4},
					             {altC6_1, altC6_2, altC6_3, altC6_4},
					             {altC7_1, altC7_2, altC7_3, altC7_4},
					             {altC8_1, altC8_2, altC8_3, altC8_4},
					             {altC9_1, altC9_2, altC9_3, altC9_4}, 
					             {altC10_1, altC10_2, altC10_3, altC10_4},
					             {altC11_1, altC11_2, altC11_3, altC11_4},
					             {altC12_1, altC12_2, altC12_3, altC12_4},
					             {altC13_1, altC13_2, altC13_3, altC13_4},
					             {altC14_1, altC14_2, altC14_3, altC14_4},
					             {altC15_1, altC15_2, altC15_3, altC15_4},
					             {altC16_1, altC16_2, altC16_3, altC16_4},
					             {altC17_1, altC17_2, altC17_3, altC17_4},
					             {altC18_1, altC18_2, altC18_3, altC18_4},
					             {altC19_1, altC19_2, altC19_3, altC19_4},
					             {altC20_1, altC20_2, altC20_3, altC20_4},
					             {altC21_1, altC21_2, altC21_3, altC21_4},
					             {altC22_1, altC22_2, altC22_3, altC22_4},
					             {altC23_1, altC23_2, altC23_3, altC23_4},
					             {altC24_1, altC24_2, altC24_3, altC24_4},
					             {altC25_1, altC25_2, altC25_3, altC25_4}
								};
	
	int *lista_respC[25] = {
				              RespCertaPergC1, RespCertaPergC2, RespCertaPergC3, RespCertaPergC4, RespCertaPergC5,
				              RespCertaPergC6, RespCertaPergC7, RespCertaPergC8, RespCertaPergC9, RespCertaPergC10,
				              RespCertaPergC11, RespCertaPergC12, RespCertaPergC13, RespCertaPergC14, RespCertaPergC15,
				              RespCertaPergC16, RespCertaPergC17, RespCertaPergC18, RespCertaPergC19, RespCertaPergC20,
				              RespCertaPergC21, RespCertaPergC22, RespCertaPergC23, RespCertaPergC24, RespCertaPergC25
						};
	
	int lista_sorteadasC[25] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	// Realizando A��o Aleat�ria: 
	
	int perguntasGeradas = 0;
	
	while (perguntasGeradas < 1){

		srand(time(NULL));
		
		int RandomPergC = rand() % 25;
		
		if (lista_sorteadasC[RandomPergC] == 0){
		
			*pergunta 		= lista_perguntasC[RandomPergC];
			*alternativa1	= lista_altC[RandomPergC][0];
			*alternativa2	= lista_altC[RandomPergC][1];
			*alternativa3	= lista_altC[RandomPergC][2];
			*alternativa4	= lista_altC[RandomPergC][3];
			
			lista_sorteadasC[RandomPergC] = 1;
			
			perguntasGeradas =+1;
		}

	}
	
}
// 

