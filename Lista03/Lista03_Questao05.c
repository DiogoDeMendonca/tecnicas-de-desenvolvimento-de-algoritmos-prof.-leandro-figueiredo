/*
Palíndromo é a frase ou palavra que se pode ler, indiferentemente, da esquerda para a direita ou vice-versa,
como por exemplo a palavra ARARA ou o nome ANA.
Escreva um programa que receba do usuário um texto
e informe se é palíndromo ou não.
Desconsidere caracteres em maiúsculo,
por exemplo, o nome Ana deve ser identificado como palíndromo.
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main(){
	
	char nome[100] = "\0";
	int i = 0;
	
	printf("..:: Palindromo ::.. \n \n");
	printf("Digite um Palindromo: \n \n");
	scanf("%s" , nome);
	getchar();
	printf("\a");
	system("CLS");
	
	for ( i ; i < strlen(nome) ; i++){
		nome[i] = toupper (nome[i]);
	}
					
	for ( i = 0 ; i < strlen(nome) ; i++){		
		if (nome[i] == nome[(strlen(nome) - 1 - i)] ){
			printf("\n \t %c == %c \n", nome[i], nome[(strlen(nome) - 1 - i)]);
			if (i == strlen(nome)-1){
				printf("\n %s E Palindromo! \n", nome);
			}
		} else {
			printf("\n \t %c != %c :( \n", nome[i], nome[(strlen(nome) - 1 - i)]);
			printf("\n \t %s NAO E Palindromo! \n", nome);
			break;
		}
	}
	return 0;
}