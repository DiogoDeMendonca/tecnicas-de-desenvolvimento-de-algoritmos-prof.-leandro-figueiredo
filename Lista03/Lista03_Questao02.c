/*
Implemente a função strcmp de string.h utilizando qualquer estrutura de repetição.
A função strcmp compara os conteúdos de duas strings e verifica se são iguais retornando 0.
*/

#include <string.h>

int main(){
	
	char stringOrigem[100] = "Teste";
	char stringDestino[100] = "Testes";
	int i = 0;

	for ( i ; i < 1 ; i++){
		(strcmp(stringDestino, stringOrigem) == 0) ? printf("Strings Identicas!") : printf("Strings Distintas!");
	}

	return 0;
}
