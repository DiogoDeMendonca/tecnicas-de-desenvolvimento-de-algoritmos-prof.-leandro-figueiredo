/*
Implemente a função strcat de string.h utilizando qualquer estrutura de repetição.
A função strcat concatena o conteúdo de uma string em outra.
DICA: Cuidado com o espaço em memória.
*/

#include <string.h>

int main(){
	
	char nome[100] = "Diogo";
	char sobrenome[100] = "Mendonca";
	char nomeCompleto[200] = "\0";
	int i = 0;

	for ( i ; i < 1 ; i++){
		strcat(nomeCompleto, nome);
		strcat(nomeCompleto, " ");
		strcat(nomeCompleto, sobrenome);
		
		puts(nomeCompleto);
	}

	return 0;
}