/*
Implemente a função strcpy de string.h utilizando qualquer estrutura de repetição.
A função strcpy copia o conteúdo de uma string para outra.
DICA: Cuidado com o espaço em memória!
*/

#include <string.h>

int main(){
	
	char stringOrigem[100] = "Teste";
	char stringDestino[100] = "\0";
	int i = 0;

	for ( ; i < 1 ; i++ ){
		strcpy(stringDestino, stringOrigem);
	}
	
	return 0;
}