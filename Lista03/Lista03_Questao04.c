/*
Implemente a função strlen de string.h utilizando qualquer estrutura de repetição.
A função strlen retorna a quantidade de caracteres em uma string.
DICA: A string termina com o caractere especial '\0'.
*/

#include <string.h>

int main(){
	
	char nome[100] = "Diogo";
	char sobrenome[100] = "Mendonca";
	char nomeCompleto[200] = "\0";
	int tamNome = 0;
	int tamSobrenome = 0;
	int tamNomeCompleto = 0;
	int i = 0;

	for ( i ; i < 1 ; i++){
		strcat(nomeCompleto, nome);
		strcat(nomeCompleto, " ");
		strcat(nomeCompleto, sobrenome);

		tamNome = strlen(nome);
		tamSobrenome = strlen(sobrenome);
		tamNomeCompleto = strlen(nomeCompleto);

		printf("Tamanho do Nome \"%s\": %d \n", nome, tamNome); 
		printf("Tamanho do Sobrenome \"%s\": %d \n", sobrenome, tamSobrenome);
		printf("Tamanho do Nome Completo \"%s\": %d \n", nomeCompleto, tamNomeCompleto);
	}

	return 0;
}